import React, {Component} from 'react';
import {Text,  TouchableOpacity, View} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {darkPurple, lightPurple, purpleTitle} from "./colors/index";
import {tag, tagWrapper} from "./styles/geralStyle";
import {type_color} from "./colors/index";


const TagButton= ({title,  onPress, color}) => {

    const getColor = ()=>{
        switch (title){
            case  'Dark':
                return  type_color.type_dark
            case  'Bug':
                return  type_color.type_bug
            case  'Grass':
                return  type_color.grass
            case  'Dragon':
                return  type_color.type_dragon
            case  'Electric':
                return  type_color.type_electric
            case  'Fairy':
                return  type_color.type_fairy
            case  'Flying':
                return  type_color.type_flying
            case  'Fire':
                return  type_color.type_fire
            case 'Water':
                return type_color.water
            case  'Ghost':
                return  type_color.type_ghost
            case  'Ground':
                return  type_color.type_ground
            case  'Ice':
                return  type_color.type_ice
            case  'Normal':
                return  type_color.type_normal
            case  'Poison':
                return  type_color.type_poison
            case  'Psychic':
                return  type_color.type_pshychic
            case  'Rock':
                return  type_color.type_rock
            case  'Steel':
                return  type_color.steel

            default:

                return null
        }

    }


    return (
        <TouchableOpacity onPress={onPress}>
            <View>
                <View
                    style={[tagWrapper, {backgroundColor: getColor()}]}>
                    <Text style={tag}>{title}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = {}

export {TagButton};