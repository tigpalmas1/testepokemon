import React, {Component} from 'react';
import {
    View, Text, Modal, TouchableOpacity, StyleSheet, Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {connect} from 'react-redux'


import {Button} from "../../components/common/Button";

import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import {dialogText} from "../../components/common/styles/geralStyle";

import {
    blackBackground, buttonsContainer, buttonsWrapper,
    cancelButtonStyle, iconStyle, titleWrapper, whiteContainer,
    whiteTitleStyle
} from "../../components/common/styles/dialogStyles";
import {Input} from "../../components/common/Input";
import {filterUpdateAction, setFilterAction, stateChangedAction} from "../../redux/actions";

import {CalcPicker} from "../../components/common/CalcPicker";
import {InputLogin} from "../../components/common/InputLogin";
import myJson from '../../components/common/constants/estados-cidades.json'
import RNPickerSelect, {defaultStyles} from 'react-native-picker-select';
import {fixedType, fixedTypeItems} from "../../components/common/constants/index";
import _ from 'lodash';
import DialogSubTitle from "../../components/common/DialogSubTitle";



class CalcDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            states: [],
            typeError: '',
            cityError: '',
            stateError: '',
            fixedTypeError: '',
        }


    }




    async componentDidMount (){

        const newArray= _.map(myJson.estados, o => _.extend({label: o.nome, value: o.nome}, o));



       await this.setState({
            states: newArray
        })

        console.log(this.state.states)

        const {searchType, consumeValue, state, city, fixedType} = this.props.filter || {}


        await this.setUF(state)
    }

    componentDidUpdate(prevProps){




        if(prevProps.filter.state!==this.props.filter.state){
            const {searchType, consumeValue, state, city, fixedType} = this.props.filter || {}
            this.setUF(state)
        }
    }




    onPressButton = () => {
        const {filter, setFilterAction, stateChangedAction} = this.props
        console.log(this.props.filter)
        if (!this.checkError()){
            setFilterAction(filter)
            stateChangedAction(true);
            this.props.dismissDialog()
        }
    }



    checkError() {
        const {searchType, value, state, city, fixedType} = this.props.filter || {}
        let errors = false;
        if (!searchType) {
            this.setState({typeError: 'Selecione o Tipo '})
            errors = true
        }

        if (!value) {
            this.setState({valueError: 'Valor do consumo necessário'})
            errors = true
        }

        if (!city) {
            this.setState({cityError: 'Selecione uma Cidade'})
            errors = true
        }

        if (!state) {
            this.setState({stateError: 'Selecione um Estado'})
            errors = true
        }

        if (!fixedType) {
            this.setState({fixedTypeError: 'Selecione um Tipo de fixação'})
            errors = true
        }
        return errors

    }

    setUF = (state)=>{
        console.log('setUF')
        console.log(state)

        this.setState({stateError: ''})
        this.props.filterUpdateAction({prop: 'state', value: state})

        console.log(this.state.states)

        var tempState = _.find(this.state.states, {nome:state});
        //console.log(tempState)


        const {cidades} = tempState ||{}
        const newCities= _.map(cidades, o => _.extend({label: o, value: o}, o));

      //  console.log(newCities)

        this.setState({cities: newCities})


    }

    getPlaceholder = () =>{
        const {searchType} = this.props.filter || {}
        if(searchType ==='kWh'){
            return 'Consumo em kWh'
        }else  if (searchType === 'modulesNumber') {
            return 'Número de Módulos'
        }else  if (searchType === 'kWp') {
            return 'Consumo em kWp'
        }
    }



    render() {
        const {typeError,cityError, stateError,fixedTypeError,consumeValueError,   saving, checked, cities, states} = this.state
        const {
            instaler,
            visible, title, text, onPositivePress,
            loading,
            cancelButton,
            loadingText,
            dismissDialog,
            iconName, category, categoryUpdate, action, filter, filterUpdateAction,
        } = this.props;



        const {searchType, value, state, city, fixedType} = filter || {}






        return (

            <Modal
                onRequestClose={() => {
                }}
                animationType='slide' transparent visible={visible}>
                <View style={blackBackground}>
                    <View style={whiteContainer}>

                        <View style={titleWrapper}>
                            <Icon
                                size={20}
                                name={'calculator'}
                                style={iconStyle}
                            />
                            <Text style={whiteTitleStyle}> {title} </Text>

                        </View>


                        <View>
                            <View style={{padding: 10}}>

                                {!instaler &&
                                <CalcPicker
                                    type={searchType}
                                    typeUpdate={(type) => {
                                        this.setState({searchTypeError: ''})
                                        filterUpdateAction({prop: 'value', value:''})
                                        filterUpdateAction({prop: 'searchType', value: type})
                                    }}
                                />
                                }


                                {!instaler &&
                                <View>
                                    <DialogSubTitle label={'Consumo: '} error={consumeValueError}/>
                                    <View style={{flexDirection: 'row', width: '100%',}}>

                                        <InputLogin
                                            keyboardType={'numeric'}
                                            borderColor={purpleTitle}
                                            maxLength={searchType === 'modulesNumber' ? 3 : null}
                                            autoCapitalize={'none'}
                                            type={'number'}
                                            error={consumeValueError}
                                            value={value}
                                            placeholder={this.getPlaceholder()}
                                            setRef={(input) => this.passwordRef = input}
                                            onSubmitEditing={() => {
                                                Keyboard.dismiss()
                                            }}
                                            onChangeText={value => {
                                                this.setState({consumeValueError: ''})
                                                filterUpdateAction({prop: 'value', value})
                                            }}
                                        />
                                    </View>
                                </View>
                                }



                                <DialogSubTitle label={'Estado: '} error={stateError}/>
                                <View style={{
                                     width: '100%', borderWidth: 1,
                                    borderRadius: 10, borderColor: stateError? 'red': purpleTitle
                                }}>
                                    <RNPickerSelect
                                        placeholder={{label: 'Selecione um Estado...', value: null, color: titleGrey,}}
                                        items={states}
                                        onValueChange={value => {
                                            console.log(value)
                                            this.setState({stateError: ''})
                                           this.setUF(value)
                                        }}
                                        style={pickerSelectStyles}
                                        value={state}
                                    />



                                </View>

                                <Text style={{color: 'red', fontWeight: '700', fontSize: 12}}>{stateError}</Text>

                                <DialogSubTitle label={'Cidade: '} error={cityError}/>
                                <View style={{
                                    flexDirection: 'row', width: '100%', marginTop: 7, borderWidth: 1,
                                    borderRadius: 10, borderColor: cityError? 'red': purpleTitle
                                }}>
                                    <RNPickerSelect
                                        placeholder={{label: 'Selecione uma Cidade...', value: null, color: titleGrey}}
                                        items={cities}
                                        onValueChange={value => {
                                            this.setState({cityError: ''})
                                            filterUpdateAction({prop: 'city', value})
                                        }}
                                        style={pickerSelectStyles}
                                        value={city}
                                    />
                                </View>

                                <Text style={{color: 'red', fontWeight: '700', fontSize: 12}}>{cityError}</Text>


                                {!instaler &&
                                <View>
                                    <DialogSubTitle label={'Tipo de Fixação: '} error={fixedTypeError}/>
                                    <View style={{
                                        flexDirection: 'row', width: '100%', marginTop: 7, borderWidth: 1,
                                        borderRadius: 10, borderColor: fixedTypeError? 'red': purpleTitle
                                    }}>
                                        <RNPickerSelect
                                            placeholder={{label: 'Selecione o tipo de fixação', value: null, color: titleGrey,}}
                                            items={fixedTypeItems}
                                            onValueChange={value => {
                                                this.setState({fixedTypeError: ''})
                                                filterUpdateAction({prop: 'fixedType', value})
                                            }}
                                            style={pickerSelectStyles}
                                            value={fixedType}
                                        />
                                    </View>
                                    <Text style={{color: 'red', fontWeight: '700', fontSize: 12}}>{fixedTypeError}</Text>

                                </View>
                                }




                                <View
                                    style={buttonsContainer}
                                >
                                    <View style={buttonsWrapper}>
                                        {cancelButton &&
                                        <TouchableOpacity
                                            onPress={dismissDialog}
                                        >
                                            <Text style={cancelButtonStyle}>FECHAR</Text>
                                        </TouchableOpacity>

                                        }


                                        <Button
                                            color={purpleTitle}
                                            disabled={saving}
                                            onPress={this.onPressButton}
                                            label={saving ? 'Salvando' : 'Calcular'}/>


                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }


}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        width: 400,
        paddingVertical: 10,
        fontWeight: '700',
        paddingHorizontal: 10,
        borderRadius: 4,
        color: titleColor,
         paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        fontWeight: '700',
        paddingHorizontal: 10,
        width: 400,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: purpleTitle,


        borderRadius: 8,
        color: titleColor,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});


const mapStateToProps = (state) => {


    return {
        filter: state.filter.filter,
    };
};
export default connect(mapStateToProps, {filterUpdateAction, setFilterAction, stateChangedAction})(CalcDialog)






