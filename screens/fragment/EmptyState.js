import React from 'react';
import {StyleSheet, Text, View, TouchableHighlight, Image, Dimensions} from 'react-native';
import {authUpdate, loginServer, facebookLogin} from "../../redux/actions";
import {connect} from 'react-redux'





import {facebookBlue, logoBlue, titleGrey} from "../../components/common/colors/index";


const {height, width} = Dimensions.get('window')

class EmptyState extends React.Component {


    onPress = () => {
        this.props.onPress()
    }


    render() {
        const {image, message, hasButton} = this.props
        const {messageStyle} = styles

        return (
            <View style={{
                alignItems: 'center',
                justifyContent: 'center',

                flexWrap: 'wrap',
                flexDirection: 'column',
                padding: 10,

            }}>

                <Image
                    style={{width: width / 4, height: width / 4,}}
                    rezise={'contain'}
                    source={image}/>

                <Text style={messageStyle}>{message}</Text>


            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    messageStyle: {
        paddingHorizontal: 20,
        textAlign: "center",
        marginTop: 10,
        fontWeight: '700',
        fontSize: 22,
        color: logoBlue
    }
});


export default EmptyState

