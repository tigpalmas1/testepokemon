import {
    ADD_ITEM,
    CLEAR_KITS_LIST,
    ESTABLISHMENT_COORDENATES_UPDATE, FETCH_KITS, FETCH_KITS_LOADING, FETCH_KITS_PROMOIONAL_LOADING,
    FETCH_KITS_PROMOTIONAL, REMOVE_ITEM, SET_KIT,

} from "./types";
import {checkResponseError, getStorage} from "../../components/common/functions/index";
import axios from 'axios'
import {baseUrl} from "../../components/common/constants/index";

export const fetchCategories = () => async (dispatch) => {


    await reloadCategories(dispatch)
};



export const addCategoryAction = (category, callback) => async (dispatch) => {
    console.log(category)

    try {
        const url =  `${baseUrl}/category`;
        const {data} = await axios.post(url, category);

        await reloadCategories(dispatch, callback, 'Categoria Adicionada com sucesso');

    } catch (e) {

        console.log(checkResponseError(e))
    }
};

export const deleteCategoryAction = (id, callback) => async (dispatch) => {

    try {
        const url =  `${baseUrl}/category/${id}`;
        const {data} = await axios.delete(url);

        await reloadCategories(dispatch, callback, 'Categoria deletada com sucesso');

    } catch (e) {

        console.log(e.message)
    }
};

const reloadCategories = async (dispatch, callback, message) =>   {
    dispatch({
        type: 'FETCH_CATEGORIES_LOADING',
        payload: {loading: true, message: 'Buscando seus produtos'}
    });

    try {
        const url =  `${baseUrl}/category?`
        const {data} = await axios.get(url);

        dispatch({
            type: 'FETCH_CATEGORIES',
            payload: data
        });

        dispatch({
            type: 'FETCH_CATEGORIES_LOADING',
            payload: {loading: false, message: ''}
        });
    } catch (e) {
        dispatch({
            type: 'FETCH_CATEGORIES_LOADING',
            payload: {loading: false, message: checkResponseError(e)}
        });
    }
};

export const fetchProductsAction = () => async (dispatch) => {


    await reloadProducts(dispatch)
};

const reloadProducts = async (dispatch, callback, message) =>   {
    dispatch({
        type: 'FETCH_PRODUCTS_LOADING',
        payload: {loading: true, message: 'Buscando seus produtos'}
    });

    try {
        const url =  `${baseUrl}?`
        const {data} = await axios.get(url);
        const {pokemon} = data ||[]


        dispatch({
            type: 'FETCH_PRODUCTS',
            payload: pokemon
        });

        dispatch({
            type: 'FETCH_PRODUCTS_LOADING',
            payload: {loading: false, message: ''}
        });
    } catch (e) {
        dispatch({
            type: 'FETCH_PRODUCTS_LOADING',
            payload: {loading: false, message: checkResponseError(e)}
        });
    }
};



export const addProductAction = (product) => async (dispatch) => {
    const {localImage} = product || {}

    try{
        const form = new FormData();
        var result = null
        if (localImage) {
            form.append('image', {uri: localImage, type: 'image/jpeg', name: 'productImage'});
        }
        form.append('object', JSON.stringify(product));
        const {data,} = await axios.post(`${baseUrl}/product`, form);
        //dispatch({type: 'ADD_PRODUCT', payload: product})
    }catch (e){
        console.log(checkResponseError(e))
    }




};






