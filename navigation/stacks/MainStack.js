import React from 'react';
import { createBottomTabNavigator} from 'react-navigation';
import HomeStack from './HomeStack'
import BudgetStack from './BudgetStack'
import PerfilStack from './PerfilStack'
import {purpleTitle} from "../../components/common/colors/index";


const MainStack =  createBottomTabNavigator({
    HomeStack,

}, {
    tabBarOptions: {
        header: null,
        activeTintColor: purpleTitle,
        inactiveTintColor: 'grey',
        style: {
            backgroundColor: 'white',
            borderTopWidth: 0,
            shadowOffset: {width: 5, height: 3},
            shadowColor: 'black',
            shadowOpacity: 0.5,
            elevation: 5

        }
    }
});

MainStack.navigationOptions = {
    header: null,

};

export default MainStack