import {
    AUTH_UPDATE, CLEAR_USER, LOGIN_LOADING, LOGIN_ERROR, LOGIN_SUCCESS, SIGNUP_LOADING, SIGNUP_SUCCESS, SIGNUP_ERROR,
    LOGOUT, AUTH_LOADING, FETCH_ESTABLISHMENT, FETCH_ACTIVE_TICKET, ESTABLISHMENT_UPDATE, TICKET_UPDATE, SET_INSTALER

} from "./types";
import {checkResponseError, getStorage, setStorage} from "../../components/common/functions/index";
import axios from 'axios';
import {baseUrl} from "../../components/common/constants/index";
import * as IntentLauncher from 'expo-intent-launcher'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'

export const authUpdate = ({prop, value}, password) => async (dispatch) => {
    dispatch({
        type: AUTH_UPDATE,
        payload: {prop, value}
    })
};


export const checkLoggedAction = (callback) => async (dispatch) => {
    dispatch({type: AUTH_LOADING})


    try {
        const data = await getStorage("data");


        const accesstoken = await getStorage("Accesstoken");
        const permission = await getStorage("permission");
        const user = await getStorage("user");

        console.log(user)
        console.log(accesstoken)

        axios.defaults.headers.common['Accesstoken'] = accesstoken;




        if (accesstoken && user && permission) {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: {
                    user: user,
                    permission: permission
                },
            })
            callback()

        } else {
            callback()
            dispatch({type: LOGIN_ERROR})
        }


    } catch (e) {
        callback()
        console.log(e)
        dispatch({type: LOGIN_ERROR})
    }


};

export const loginServerAction = (authObject, callback) => async (dispatch) => {

    dispatch({type: LOGIN_LOADING})
    try {
        const {data, headers} = await axios.post(`${baseUrl}/login`, authObject);

        const {user, permission, token, auth} = data || {}

        console.log(data)

        const {passwordReseted} = auth || {}
        await setStorage("user", user);
        await setStorage("Accesstoken", token);
        await setStorage("permission", permission);
        axios.defaults.headers.common['Accesstoken'] = token;



        dispatch({
            type: LOGIN_SUCCESS,
            payload: {
                user: user,
                permission: permission
            },
        })


        callback(passwordReseted)

    } catch (e) {
        console.log(e.response.status)
        dispatch({
            type: LOGIN_ERROR,
            payload: checkResponseError(e)
        })
    }
};


export const signupServer = (auth, callback) => async (dispatch) => {

    console.log(auth)

    dispatch({type: SIGNUP_LOADING})
    try {
        const {data, headers} = await axios.post(`${baseUrl}/register`, auth);
        const {user, permission, token} = data || {}
        await setStorage("user", user);
        await setStorage("Accesstoken", token);
        await setStorage("permission", permission);
        axios.defaults.headers.common['Accesstoken'] = token;

        console.log(data)

        dispatch({
            type: LOGIN_SUCCESS,
            payload: {
                user: user,
                permission: permission
            },
        })

        callback()

    } catch (e) {
        console.log(e)
        dispatch({
            type: SIGNUP_ERROR,
            payload: checkResponseError(e)
        })
    }
};


export const passwordResendAction = (email, callbackReset) => async () => {
    let url = `${baseUrl}/reset-password?`;

    try {
        await axios.post(url, {"email": email});


        callbackReset("um senha provisória foi enviado para o seu email")

    } catch (e) {
        callbackReset(checkResponseError(e))
    }
};

export const changePasswordAction = (auth, callbackReset) => async () => {
    const {password: currentPassword, newPassword} = auth ||{}


    let url = `${baseUrl}/change-password?`;

    try {
        await axios.post(url, {currentPassword, newPassword});


        callbackReset("Senha alterada com sucesso!")

    } catch (e) {
        callbackReset(checkResponseError(e))
    }
};

export const logoutAction = () => async (dispatch) => {

    try {
        await setStorage("user", null);
        await setStorage("permission", null);
        await setStorage("accesstoken", null);
        dispatch({type: LOGOUT})


    } catch (e) {
        alert(checkResponseError(e))
    }


};






