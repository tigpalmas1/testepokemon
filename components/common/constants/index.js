
export const baseUrl = 'https://www.canalti.com.br/api/pokemons.json'


export const acceptLoginTherms= 'O vendedor solar não se responsabiliza pela instalação e homologação dos sistemas fotovoltaicos. Estou ciente que a responsabilidade cabe ao instalador.'
export const intalerTherms= 'O cadastro do instalador é apenas liberado para empresas com CNAE de instalação e possui o custo de R$19,90 mensal. ' +
    '' +
    'O Conteúdo preenchido será avaliado pela nossa equipe e será aprovado após a confirmaçaõ das informações'

//export const baseUrl= 'localhost:3000'



export  const fixedTypeItems = [
    {
        label: 'Telha Cerâmica',
        value: 'Telha Cerâmica',
    },
    {
        label: 'Telha Fibromadeira',
        value: 'Telha Fibromadeira',
    },
    {
        label: 'Telha Fibrometal',
        value: 'Telha Fibrometal',
    },
    {
        label: 'Telha Metálica',
        value: 'Telha Metálica',
    },
    {
        label: 'Sem Estrutura',
        value: 'Sem Estrutura',
    },
];
