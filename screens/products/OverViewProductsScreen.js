import React,{useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    Dimensions,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import {Header} from "../../components/Header";
import ProductItem from "./ProductItem";
import {connect} from "react-redux";
import {fetchProductsAction} from "../../redux/actions/product_action";
import {titleGrey} from "../../components/common/colors/index";


const   OverViewProductsScreen = (props) => {
    const products = useSelector(state => state.product.products);
    console.log(products)

    const {loading, message, navigation, fetchProductsAction} = props || {}
    const {navigate} = navigation || {}


    useState(()=>{
        fetchProductsAction();
    },[])


    const renderItem = (item, index) => {
        console.log('entrou aqui')
        return (


            <ProductItem
                product={item}

                onPressCard={() => {

                    navigate('productDetail', {product: item})
                }}/>

        )
    }


    return (


        <View style={styles.container}>
            <Header
                cartIcon
                onCartPress={()=>navigation.navigate('cart')}
                onPress={() => {
                    navigation.goBack(null)
                }}

                title={''}


            />


            {/*  {message != '' && kits.length === 0 &&
                <View style={emptyContainer}>
                    <EmptyState
                        message={message}
                    />
                </View>
                }
*/}

            <View style={{marginHorizontal: 20, marginTop:-20}}>
                <Text style={{fontSize: 30, fontWeight: '700'}}>Pokédex</Text>
                <Text style={{fontSize: 14, fontWeight: '700', color: titleGrey}}>
                    Procure por pokemons pelo nome ou usando o número nacional pokédex
                </Text>
            </View>



            <FlatList
                numColumns={1}
                refreshing={false}
                onRefresh={() => console.log('refreshing')}
                keyExtractor={(item, index) => item._id}
                showsVerticalScrollIndicator={false}
                style={{flex: 1,}}
                data={products}
                onEndReached={this.handleLoadMore}
                onEndThreshold={0}
                renderItem={({item, index}) => renderItem(item, index)}


            />


        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",


    },
    header: {
        marginTop: 20,
        height: 200,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },

});




export default connect(null, {fetchProductsAction})(OverViewProductsScreen)


