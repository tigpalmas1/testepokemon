import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {bug, grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


import {Card,} from 'react-native-paper';
import {
    addCartItemAction
} from "../../redux/actions";
import {useDispatch, useSelector} from 'react-redux';
import {RBText} from "../../components/RBText";
import {EBTExt} from "../../components/EBText";
import {formataDinheiro} from "../../components/common/functions/index";
import {TagButton} from "../../components/common/TagButton";
import {background_color, } from '../../components/common/colors/index';

const width = (Dimensions.get('window').width) / 2 - 5;
const WIDTH = Dimensions.get('window').width;


const ProductItem = (props) => {
    const {onPressCard, product} = props || {};
    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart.cart);

    const {name, type, img, num} = product || {}
    console.log(type[0])

    const getColor = ()=>{
        switch (type[0]){
            case  'Dark':
                return  background_color.type_dark
            case  'Bug':
                return  background_color.type_bug
            case  'Grass':
                return  background_color.grass
            case  'Dragon':
                return  background_color.type_dragon
            case  'Electric':
                return  background_color.type_electric
            case  'Fairy':
                return  background_color.type_fairy
            case  'Flying':
                return  background_color.type_flying
            case  'Fire':
                return  background_color.type_fire
            case 'Water':
                return background_color.water
            case  'Ghost':
                return  background_color.type_ghost
            case  'Ground':
                return  background_color.type_ground
            case  'Ice':
                return  background_color.type_ice
            case  'Normal':
                return  background_color.type_normal
            case  'Poison':
                return  background_color.type_poison
            case  'Psychic':
                return  background_color.type_pshychic
            case  'Rock':
                return  background_color.type_rock
            case  'Steel':
                return  background_color.steel

            default:

                return null
        }

    }

    return (

        <TouchableOpacity
            style={{flex: 1, marginTop: 20, marginHorizontal: 20}}
            onPress={onPressCard}>
            <Card style={{
                elevation: 11,
                borderRadius: 10,
                backgroundColor:getColor(),
                alignItems: 'center',
                height: 150,
                width: '100%',
                margin: 2,

                flex: 1
            }}>

                <View style={{   height: 150,
                    width: '100%', flexDirection: 'row', alignItems: 'center', padding: 20 }}>
                   <View style={{flex: 5, height: 50}}>
                       <Text style={{fontWeight: '700'}}>#{num}</Text>
                       <Text style={{fontSize: 28, color: 'white', fontWeight: '700'}}>{name}</Text>
                       <View style={{flexDirection:'row'}}>
                           {type.map(el => (
                              <TagButton title={el}/>
                               ))}
                       </View>

                   </View>
                   <View style={{flex: 5, marginTop: -40 }}>
                      < Image
                       reziseMode={'resize'}
                       style={{
                       flex: 1, width: null,
                   }}
                       source={{uri: img}}/>
                   </View>
                </View>




            </Card>
        </TouchableOpacity>





    )


}

const styles = StyleSheet.create({});


export default ProductItem
