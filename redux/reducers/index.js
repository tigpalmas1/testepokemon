import {combineReducers} from 'redux';


import auth from './auth_reducer'
import instaler from './instaler_reducer'
import kit from './kit_reducer'
import user from './user_reducer'
import filter from './filter_reducer'
import order from './order_reducer'
import product from './product_reducer'
import cart from './cart_reducer'
import address from './address_reducer'



export default combineReducers({
    address,
    auth,
    instaler,
    user,
    kit,
    order,
    filter,
    product,
    cart
});