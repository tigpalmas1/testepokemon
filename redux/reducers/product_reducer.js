import {

    FETCH_KITS_LOADING, FETCH_KITS, LOGOUT, CLEAR_KITS_LIST, SET_KIT, ADD_ITEM, REMOVE_ITEM,
    FETCH_KITS_PROMOIONAL_LOADING, FETCH_KITS_PROMOTIONAL,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    loading: false,
    categories: [],
    message: '',
    products: [



    ],





}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {
        case 'FETCH_CATEGORIES_LOADING':
            return {
                ...state,
                loading: payload.loading, message: payload.message
            }

        case 'FETCH_CATEGORIES':
            return {
                ...state,
                categories:payload
            }

        case 'FETCH_PRODUCTS':
            return {
                ...state,
                products:payload
            }


        case 'ADD_PRODUCT':
            return {
                ...state,
                products:[
                    ...state.products,
                    payload

                ]
            }
        case REMOVE_ITEM:
            return {
                ...state,
                kit:{
                    ...state.kit,
                    itemId: state.kit.itemId.map(
                        (item, i) => item._id === payload ? {...item, quantity: item.quantity-1}: item
                    )
                }
            }



        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

