import React, {Component} from 'react';
import {Text,  TouchableOpacity, View, ActivityIndicator, Alert} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {darkPurple, grey, lightPurple, purpleTitle} from "./colors/index";
import {tag, tagWrapper} from "./styles/geralStyle";
import {requestStampStatus} from "../../redux/actions/stamps_action";
import {connect} from "react-redux";



class Stamp extends React.Component{

    constructor(props) {
        super(props)

        this.state={
            loading: false
        }

    }







    setColor = (stamp) => {
        const status = stamp.get('status')
        console.log(status)
        if (status === 'Carimbar') {
            return grey
        } else if (status === 'Carimbo Solicitado') {
            return 'orange';
        } else if (status === 'Carimbado') {
            return 'green'
        }
    }

    requestStamp = (item, stamp) => {
        const {establishment} = this.props
       if(establishment){
            this.carimbarEstablishment(item, stamp)
       }else{
           this.carimbarUser(item, stamp )
       }

    }

    carimbarUser = (item, stamp)=>{
        const status = stamp.get('status')
        if (status === 'Carimbar') {
            this.setState({loading: true})
            this.props.requestStampStatus("Carimbo Solicitado","Carimbo Solicitado", item, stamp, this.callback)
        } else if (status === 'Carimbo Solicitado') {
            this.callback('Você deve aguardar uma resposta do estabelecimenot')
        }
        else if (status === 'Carimbado') {
            this.callback('Esse Item já está carimbado')
        }
    }

    carimbarEstablishment = (item, stamp)=>{
        const status = stamp.get('status')
        if (status === 'Carimbar') {
            this.callback('Somente Usuário pode soliccitar carimbos')

        } else if (status === 'Carimbo Solicitado') {
            this.setState({loading: true})
           this.props.requestStampStatus("Carimbado","Ok", item, stamp, this.callback)
        }
        else if (status === 'Carimbado') {
            this.callback('Esse Item já está carimbado')
        }
    }

    callback = (message) => {
        this.setState({loading: false})
        Alert.alert("", message);

    }



    render(){

        const {loading} = this.state
        const {stamp,  onPress, color, item} = this.props

        return (
            <TouchableOpacity onPress={loading? null : ()=>this.requestStamp(item, stamp)}>
                <View

                    style={{
                        alignItems: 'center', justifyContent: 'center',
                        width: 80, height: 80, borderRadius: 40,
                        marginRight: 5, marginBottom: 5,
                        backgroundColor: this.setColor(stamp)}}
                >

                    {!loading &&
                    <Text style={{textAlign: 'center', fontWeight: '700'}}>{stamp.get('status')}</Text>
                    }


                    {loading &&
                    <View>
                        <ActivityIndicator
                            color={purpleTitle}
                        ></ActivityIndicator>
                    </View>
                    }

                </View></TouchableOpacity>
        )
    }



}

const styles = {}
const mapStateToProps = (state) => {
    return {
        stamps: state.stamp.stamps
    }
}


export default connect(mapStateToProps, {requestStampStatus})(Stamp)


