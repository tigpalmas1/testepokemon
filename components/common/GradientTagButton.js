import React, {Component} from 'react';
import {Text,  TouchableOpacity} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {darkPurple, gradientColor, lightPurple, purpleTitle} from "./colors/index";
import {tag, tagWrapper} from "./styles/geralStyle";



const GradientTagButton = ({title, iconName, onPress}) => {


    return (
        <TouchableOpacity onPress={onPress}>

            <LinearGradient
                colors={gradientColor}
                start={[0,0]}
                end={[1,0]}
                style={tagWrapper}>

                <Icon
                    style={{marginRight: 5, marginLeft: 2}}
                    size={10}
                    color={'white'}
                    name={iconName}
                />
                <Text
                    style={tag}>{title} </Text>
            </LinearGradient>
        </TouchableOpacity>
    )
};



export {GradientTagButton};