import {grey, lightPurple, purpleTitle, shimmerPurpe, titleColor, titleGrey, shimmerGrey} from "../colors/index";


//GERAL STYLES
export const blackBackground =   {
    position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,

        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'

}
export const   whiteContainer= {
        backgroundColor: 'white',
        width: '90%',
        borderRadius: 12,
        overflow: 'hidden',

    }

export const     titleWrapper =  {
        width: '100%',
        padding: 10,
        backgroundColor: purpleTitle,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }

export const   whiteTitleStyle = {color: 'white', fontWeight: '700', fontSize: 18}
export const    buttonsContainer =  {width: '100%', alignItems: 'flex-end', marginTop: 18, }
export const buttonsWrapper =  {flexDirection: 'row', alignItems: 'center', }
export const  cancelButtonStyle = {fontWeight: '900', marginRight: 20, color: purpleTitle, }
export const  iconStyle=  {color: "white", marginRight: 5}



