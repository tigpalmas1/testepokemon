import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {logoBlue, purpleTitle, titleColor} from "./colors/index";


const TransparentHeader = ({title, onPress,onPressShare,onPressLike, share, like, saving,arrowColor, foundObject}) => {
    const {header,} = styles;


    renderIcon = ()=>{

         if(saving){
             return (
                 <View>
                     <ActivityIndicator
                         color={purpleTitle}
                     ></ActivityIndicator>
                 </View>
             )
         }else{
             return (
                 <TouchableOpacity onPress={onPressLike}>
                     <Icon
                         color='white'
                         size={24}
                         name={foundObject ? "heart" : "heart-outline"}
                         style={[foundObject ? {color: 'red'} : {color: 'grey'}]}
                     />
                 </TouchableOpacity>
             )
         }
    }



    return (

        <View style={header}>
            <View style={{flexDirection: 'row', alignItems:'center'}}>
                <TouchableOpacity onPress={onPress}>
                    <Icon
                        color={arrowColor || 'white'}
                        size={24}
                        name="arrow-left"
                    />
                </TouchableOpacity>
                <View><Text style={{color:logoBlue, fontWeight:'700', marginLeft:10, fontSize: 18}}>{title}</Text></View>
            </View>




            <View style={{flexDirection:'row'}}>
                {share &&
                <View>
                    <TouchableOpacity onPress={onPressShare}>
                        <Icon
                            color='white'
                            size={24}
                            name="share"
                        />
                    </TouchableOpacity>
                </View>
                }
                {like &&
                <View  style={{marginLeft:5, alignItems:'center', justifyContent:'center'}}>
                    {this.renderIcon()}

                </View>
                }

            </View>


        </View>

    )
}

const styles = {
    header: {
        flexDirection: 'row',
        width: '100%',
        height: 80,
        padding: 20,
        justifyContent: "space-between",
        alignItems: 'center',


    },

}

export {TransparentHeader};