import React, {useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {useSelector, useDispatch} from 'react-redux';

import {Card,} from 'react-native-paper';
import {ImagePicker, LinearGradient, Permissions} from 'expo'
import {
    formataDinheiro
    , getNationalValue,
} from "../../components/common/functions/index";
import {TagButton} from "../../components/common/TagButton";


export default AddressSelected = (props) => {

    const listAddress = useSelector(state => state.address.listAddress);
    console.log(listAddress)

    const {removeCategoryPress, onPressAddProduct} = props || {};
    const {address} = props || {};
    const {_id, neighborhood, cep, city, state, number, street} = listAddress[0] || {}


    const [ìsExpanded, setìsExpanded] = useState(false);


    return (
        <View style={{width: '100%',}}>
            <Text>{street},{number} - {state} {cep}</Text>
        </View>

    )


}

const styles = StyleSheet.create({});



