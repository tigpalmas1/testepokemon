import React,{useEffect, useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    Dimensions,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import {fetchCategories, deleteCategoryAction, addCategoryAction} from "../../redux/actions";
import {connect} from "react-redux";

import {Header} from "../../components/Header";

import CategoryItem from "./CategoriItem";
import {ConfirmationDialog} from "../dialogs/ConfirmationDialog";
import AddCategoryDialog from "../dialogs/AddCategoryDialog";
import {emptyContainer} from "../../components/common/styles/geralStyle";
import EmptyState from "../fragment/EmptyState";



const ProductsListScreen = (props) => {
    const {fetchCategories, deleteCategoryAction, addCategoryAction} = props||{}
    const categories = useSelector(state => state.product.categories);
    const message = useSelector(state => state.product.message);
    const loading = useSelector(state => state.product.loading);
    const[confirmationDialog, setConfirmationDialog] = useState(false);
    const[addDialog, setAddDialog] = useState(false);
    const[category, setCategory] = useState(null);



    const {navigation} = props ||{}

    useEffect(() =>{
        fetchCategories()
    },[])


    const deleteCategory = ()=>{
        console.log(category)
        setConfirmationDialog(false)
        deleteCategoryAction(category._id);
    }

    const addCategory = (name)=>{
        const category = {name}
        setAddDialog(false)
        addCategoryAction(category);
    }

    renderItem = (item, index) => {

        return (


            <View style={{marginTop:5, marginHorizontal: 10}}>
            <CategoryItem
                removeCategoryPress={()=>{
                    setCategory(item)
                    setConfirmationDialog(true)}}
                item={item}
                onPressAddProduct={(item)=>navigation.navigate('createEditProduct', {item})}
               />

            </View>
        )
    }


    return (


        <View style={styles.container}>
            <Header
                iconRigthName={'plus'}
                onIconPress={()=>setAddDialog(true)}
                backButton
                onPress={() => {
                    navigation.goBack(null)
                }}

                title={'Gerenciar seus produtos'}


            />


              {message != '' && categories.length === 0 &&
                <View style={emptyContainer}>

                    <EmptyState
                        message={message}
                    />
                </View>
                }



            <FlatList

                refreshing={false}
                onRefresh={() => console.log('refreshing')}
                keyExtractor={(item, index) => item._id}
                showsVerticalScrollIndicator={false}
                style={{flex: 1,}}
                data={categories}
                onEndReached={this.handleLoadMore}
                onEndThreshold={0}
                renderItem={({item, index}) => this.renderItem(item, index)}


            />

            <ConfirmationDialog
                    onPressConfirm={()=>deleteCategory()}
                    visible={confirmationDialog}
                    onPressCancel={()=>setConfirmationDialog(false)}
            />

            <AddCategoryDialog
                titlee="Adicionar Categoria"
                onPressConfirm={(name)=>addCategory(name)}
                visible={addDialog}
                onPressCancel={()=>setAddDialog(false)}
            />


        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",


    },
    header: {
        marginTop: 20,
        height: 200,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },

});





export default connect(null, {fetchCategories, deleteCategoryAction, addCategoryAction})(ProductsListScreen)

