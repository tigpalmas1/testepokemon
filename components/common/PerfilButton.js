import React, {Component} from 'react';
import { Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {RBText} from "../RBText";


const PerfilButton = ({onPress, label, iconName}) => {

    return (

        <TouchableOpacity
            onPress={onPress}>
            <View style={{marginTop: 10, flexDirection: 'row'}}>

                <RBText style={{fontSize: 16, fontWeight: '700', color: 'grey'}}>{label}</RBText>
            </View>
        </TouchableOpacity>
    )
}



export {PerfilButton};