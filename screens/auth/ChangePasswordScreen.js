import React,{useState} from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Dimensions,
    Picker,
    ScrollView,
    Keyboard,
    RefreshControl
} from 'react-native';

import {connect} from 'react-redux'
import {authUpdate, changePasswordAction, validationEmailResendAction} from "../../redux/actions";
import {darkGrey, facebookBlue, logoBlue, purpleTitle, titleGrey} from "../../components/common/colors/index";
import {titleStyle} from "../../components/common/styles/loginSignupStyle";
import {PASSWORD_RECOVER, RESEND_VALIDATION_EMAIL} from "../../components/common/constants/index";
import {InputLogin} from "../../components/common/InputLogin";
import {Button} from "../../components/common/Button";
import {TransparentHeader} from "../../components/common/TransparentHeader";
import {Card,} from 'react-native-paper';


const {height, width} = Dimensions.get('window')

const ChangePasswordScreen = (props)=> {

    const {auth, authUpdate, navigation, changePasswordAction} = props ||{}

    const [passwordError, setPasswordError] = useState('')
    const [newPasswordError, setnewPasswordError] = useState('')
    const [confirmNewPasswordError, setconfirmNewPasswordError] = useState('')
    const [saving, setSaving] = useState('')
    const [errorMessage, seterrorMessage] = useState('')


    const {password, newPassword, confirmNewPassword, } = auth


    const onLoginPress = () => {

        if (!checkError()) {
            changePasswordAction(auth, callbackChange)
        }
    }

    callbackChange = (message) =>{
        setSaving(false)
        Alert.alert(message);
        navigation.goBack(null)
    }


    const checkError = () => {

        let errors = false;

        if (!password) {
           setPasswordError('Senha Obrigatória');
            errors = true
        }
        if (password && password.length < 8 || password.length > 14) {
           setPasswordError('Senha deve ter entre 8 e 14 caracteres');
            errors = true
        }

        if (!newPassword) {
           setnewPasswordError('Nova Senha obrigátoria');
            errors = true
        }
        if (newPasswordError && newPassword.length < 8 || password.length > 14) {
            setnewPasswordError( 'Senha deve ter entre 8 e 14 caracteres');
            errors = true
        }

        if (!confirmNewPassword) {
            setconfirmNewPasswordError('Repita a Nova Senha obrigátoria');
            errors = true
        }
        if (newPassword !== confirmNewPassword) {
            setconfirmNewPasswordError( 'Nova senha e confirme nova senha não conferem');
            errors = true
        }
        return errors

    }

        const {container, content,  section, pickerWrapper, pickkerText, picker} = styles



        return (
            <View style={{flex: 1, backgroundColor: purpleTitle}}>

                <View>

                    <TransparentHeader

                        arrowColor={'black'}
                        backButton
                        onPress={() => {
                           navigation.goBack(null)
                        }}
                        title={'Alterar Senha'}
                    />
                </View>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={saving}
                            onRefresh={null}
                        />
                    }>
                    <View style={content}>

                        <View style={{flex: 1, marginTop: 60,}}>


                            <Card style={{elevation: 11, width: '100%', marginTop: 20, paddingHorizontal: 10, paddingVertical: 30}}>
                            <View style={{flexDirection: 'row', width: '100%', marginTop: 20}}>

                                <InputLogin
                                    borderColor={titleGrey}
                                    icon={"lock"}
                                    label={'Senha Atual'}
                                    autoCapitalize={'none'}
                                    secureTextEntry
                                    error={passwordError}
                                    value={password}
                                    placeholder={'Senha Atual'}
                                    setRef={(input) => this.passwordRef = input}
                                    onSubmitEditing={() => {
                                        Keyboard.dismiss()
                                    }}
                                    onChangeText={value => {
                                        setPasswordError('')
                                        authUpdate({prop: 'password', value})
                                    }}

                                />
                            </View>

                                <View style={{flexDirection: 'row', width: '100%',}}>

                                    <InputLogin
                                        borderColor={titleGrey}
                                        icon={"lock"}
                                        label={'Nova Senha'}
                                        autoCapitalize={'none'}
                                        secureTextEntry
                                        error={newPasswordError}
                                        value={newPassword}
                                        placeholder={'Nova senha'}
                                        setRef={(input) => this.passwordRef = input}
                                        onSubmitEditing={() => {
                                            Keyboard.dismiss()
                                        }}
                                        onChangeText={value => {
                                            setnewPasswordError('')
                                            authUpdate({prop: 'newPassword', value})
                                        }}

                                    />
                                </View>

                                <View style={{flexDirection: 'row', width: '100%',}}>

                                    <InputLogin
                                        borderColor={titleGrey}
                                        icon={"lock"}
                                        label={'Nova Senha'}
                                        autoCapitalize={'none'}
                                        secureTextEntry
                                        error={confirmNewPasswordError}
                                        value={confirmNewPassword}
                                        placeholder={'Repita a nova senha'}
                                        setRef={(input) => this.passwordRef = input}
                                        onSubmitEditing={() => {
                                            Keyboard.dismiss()
                                        }}
                                        onChangeText={value => {
                                            setconfirmNewPasswordError('')
                                            authUpdate({prop: 'confirmNewPassword', value})
                                        }}

                                    />
                                </View>


                            <View style={{width: '100%', alignItems: 'center', marginTop: 20}}>
                                <Text style={{color: 'red', fontWeight: '700'}}>{errorMessage}</Text>
                            </View>

                            </Card>



                            <View style={{marginTop: 40}}>

                                <Button
                                    color={logoBlue}
                                    disabled={saving}
                                    onPress={onLoginPress}
                                    label={saving? 'Alterando senha...': 'Alterar'}/>
                            </View>





                        </View>
                    </View>
                </ScrollView>


            </View>
        );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: purpleTitle
    },
    content: {
        flex: 1,
        paddingHorizontal: 20
    },


    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}
});


const mapStateToProps = (state) => {

    return {
        auth: state.auth.auth,

    }
}


export default connect(mapStateToProps, {
    changePasswordAction,
    validationEmailResendAction,
    authUpdate
})(ChangePasswordScreen)



