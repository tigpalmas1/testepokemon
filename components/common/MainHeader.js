import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {purpleTitle} from "./colors/index";
import {LinearGradient} from 'expo';



const MainHeader = (props) => {
    const {header, headerContainer} = styles;
    const {leftButton, shareButton, saving,  foundObject, menuPress, children} = props


    return (

        <View style={{position: 'absolute', width: '100%'}}>
            <LinearGradient
                colors={['#00000080', '#00000000']}
                style={{flex: 1}}>
                <View style={headerContainer}>
                    <View>
                        <TouchableOpacity
                            sytle={{width: 40, height: 40}}
                            onPress={leftButton}>
                            <Icon style={{}}color={"white"} size={24} name={"arrow-left"} />
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>

                        <TouchableOpacity
                            onPress={shareButton}
                        >
                            <Icon
                                style={{marginRight: 20}}
                                color={'white'}
                                size={30}
                                name="share"
                            />
                        </TouchableOpacity>
                        {children}

                        <TouchableOpacity
                            onPress={menuPress}>
                            <Icon
                                style={{marginLeft: 20}}
                                color={'white'}
                                size={24}
                                name={"dots-vertical"}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
            </LinearGradient>

        </View>

    )
}

const styles = {
    header: {
        flexDirection: 'row',
        width: '100%',
        height: 80,
        padding: 20,
        justifyContent: "space-between",
    },
    headerContainer: {
        width: '100%',
        height: 60,
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 20
    }
}

export {MainHeader};