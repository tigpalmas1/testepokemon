import React, {useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Keyboard,
    ScrollView,
    ActivityIndicator,
    TouchableOpacity,
} from 'react-native';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';


import {
    addProductAction
} from "../../redux/actions/index";
import {useDispatch} from 'react-redux';


import {Header} from "../../components/Header";
import {
    buttonWrapperEditProfile,
    errorMessageContainer, errorMessageStyle, imageContainer, imagePickerContainer,
    nameLastNameContainer
} from "../../components/common/styles/perfilStyle";
import {grey, logoBlue, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import {Button} from "../../components/common/Button";
import {Card,} from 'react-native-paper';
import {Input} from "../../components/common/Input";


const CreateEditProductScreen = (props) => {

    const dispatch =useDispatch();

    const {navigation} = props;

    const {item} = navigation.state.params || {};

    const [saving, setSaving] = useState(false);
    const [product, setProduct] = useState({
        name: '',
        imageUrl: 'https://3eaf214443cb92a1.cdn.gocache.net/wp-content/uploads/2019/08/free-happy-woman-raising-arms-picture-id1046932116-760x450.jpg',
        price: 50,
        categoryId: item._id
    });

    const {name, imageUrl, value, description} = product || {};




    const checkError = () => {
        let errors = false;
        console.log(user)


        return errors

    };


    const pickImage = async (type) => {
        let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('Você deve liberar as permissões para acessar a camera');
        } else {
            let result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3],
            });
            if (!result.cancelled) {

                const value = result.uri;
                setProduct({...product, imageUrl: value, localImage:value })
                console.log(value)

                /**/
            } else {
                console.log('aqui')
            }
        }
    };


    const pressButton = () => {
        dispatch(addProductAction(product)),


            navigation.goBack(null)

    };


    const {content, container} = styles;


    return (
        <View style={container}>
            <View>

                <Header

                    backButton
                    onPress={() => {
                        navigation.goBack(null)
                    }}
                    title={item ? `Adicionar em ${item.name}` : "Cadastrar Produto"}
                />
            </View>
            <ScrollView>
                <View>
                    <View style={content}>

                        <View style={{alignItems: 'center'}}>


                        </View>

                        <View style={{
                            overflow: 'hidden',
                            backgroundColor: 'white',
                            borderRadius: 10,
                            overflow: 'hidden'
                        }}>
                            <TouchableOpacity onPress={() => pickImage('banner')}>
                                <Card.Cover
                                    source={
                                        {uri: imageUrl}
                                    }>

                                </Card.Cover>


                            </TouchableOpacity>


                        </View>


                        <View>
                            <View>
                                <Card style={{
                                    elevation: 11,
                                    width: '100%',
                                    paddingVertical: 10,
                                }}>


                                    <View style={nameLastNameContainer}>

                                        <Input


                                            label={'Nome do produto '}
                                            placeholder={'Digite um nome'}
                                            value={name}

                                            onChangeText={value => {

                                                setProduct({...product, name: value})
                                            }}

                                        />
                                    </View>

                                    <View style={nameLastNameContainer}>

                                        <Input

                                            label={'Descrição'}
                                            value={description}
                                            multiline
                                            onChangeText={value => {
                                                setProduct({...product, description: value})
                                            }}

                                        />
                                    </View>

                                    <View style={nameLastNameContainer}>

                                        <Input


                                            label={'Valor'}
                                            value={value}
                                            keyboardType={'number-pad'}
                                            onChangeText={value => {
                                                setProduct({...product, value: parseFloat(value)})
                                            }}

                                        />
                                    </View>
                                </Card>
                            </View>
                        </View>


                        <View style={{marginTop: 40, alignItems: 'center'}}>


                            <Button
                                disabled={saving}
                                color={logoBlue}
                                onPress={pressButton}
                                label={saving ? "Salvando, aguarde..." : 'Cadastrar'}/>

                        </View>


                    </View>
                </View>


            </ScrollView>

        </View>


    );

};

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    content: {
        flex: 1,
        padding: 10,
    },
    title: {
        fontWeight: '700', fontSize: 16,
    },
    inputWrapper: {
        margin: 3,
        width: '100%',

        borderBottomWidth: 0.5,
        borderColor: purpleTitle,
        padding: 3,

    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',
    },
    section: {marginTop: 20},
    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}

});


export default CreateEditProductScreen


