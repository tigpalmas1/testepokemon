import React from 'react';
import { Text } from 'react-native';

export function FText(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'spf-pro' }]} />
  );
}
