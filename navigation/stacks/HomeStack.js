import React from 'react';
import OverViewProductsScreen from "../../screens/products/OverViewProductsScreen";
import ProductDetailScreen from "../../screens/products/ProductDetailScreen";

import {createStackNavigator} from 'react-navigation';
import TabBarIcon from "../../components/TabBarIcon";
import CartScreen from "../../screens/cart/CartScreen";



const HomeStack = createStackNavigator({

    explorer: {screen: OverViewProductsScreen, navigationOptions: {header: null},},
    cart: {screen: CartScreen, navigationOptions: {header: null},},
    productDetail: {screen: ProductDetailScreen, navigationOptions: {header: null},},

}, {
    navigationOptions: {
        header: null,
    },
});

HomeStack.navigationOptions =({navigation}) =>{

    let tabBarVisible = true;
    if(navigation.state.index >0){
        tabBarVisible = false;
    }

    return {
        navigationOptions: {header: null},
        tabBarVisible,
        tabBarLabel: 'Explore',
        tabBarIcon: ({ focused }) => (


            <TabBarIcon
                label={'Explore'}
                focused={focused}
                name={'magnify'}
            />
        ),
    };
};

export default HomeStack