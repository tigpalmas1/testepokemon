import React,{useState}  from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { Card,} from 'react-native-paper';
import {ImagePicker, LinearGradient, Permissions} from 'expo'
import {
    formataDinheiro
    , getNationalValue,
    } from "../../components/common/functions/index";
import {TagButton} from "../../components/common/TagButton";


export default AddressItem = (props) => {

        const {removeCategoryPress, onPressAddProduct} = props || {};
        const {address} = props || {};
    const {_id, neighborhood, cep, city, state, number, street} = address || {}


    const [ìsExpanded, setìsExpanded] = useState(false);



    return (

        <TouchableWithoutFeedback onPress={()=>setìsExpanded(!ìsExpanded)}>
            <Card style={{elevation: 11, width: '100%',   }}>

                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{padding: 10, flex: 2}}>
                            <View>
                                <Text style={{
                                    fontWeight: '700',
                                    fontSize: 14,
                                    color: titleGrey
                                }}> {street}, {number}</Text>
                            </View>
                        <View>
                            <Text style={{
                                fontWeight: '700',
                                fontSize: 12,
                                color: titleGrey
                            }}> {city} - {state} {cep} </Text>
                        </View>
                    </View>

                    <View style={{paddingVertical: 10, flexDirection:'row', paddingHorizontal: 20}}>
                        <TouchableOpacity
                            onPress={removeCategoryPress}
                        >
                            <Icon
                                size={20}
                                name={'check'}
                            />

                        </TouchableOpacity>

                    </View>
                </View>





            </Card>
        </TouchableWithoutFeedback>

    )


}

const styles = StyleSheet.create({});



