import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";
import {TextInputMask} from 'react-native-masked-text'


const InputMask = ({label, value, onChangeText, type,  placeHolder, getRawValue, error, options, setRef, borderColor}) => {

    const { inputWrapper, labelStyle, inputStyle} = styles

    return (
        <View style={{ marginTop: 5, flex: 1}}>

                <Text style={[labelStyle,{color: error? 'red': titleGrey}]}>{label}</Text>
                <Text style={{color: 'red', fontWeight: '900', fontSize: 8, marginLeft:5}}>{error}</Text>


            <View style={[inputWrapper,{  borderColor: error? 'red': borderColor,} ]}>
                <TextInputMask
                    ref={setRef}
                    style={inputStyle}
                    value={value}

                    placeholder={placeHolder}
                    onChangeText={onChangeText}
                    returnKeyType={'done'}
                    underlineColorAndroid='transparent'
                    refInput={(ref) => this.myDateText = ref}
                    type={type}
                    getRawValue={getRawValue}
                    options ={options}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    inputWrapper: {
        margin: 2,
        width: '100%',
        borderBottomWidth: 0.5,

        padding: 2,

    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',


    },
    inputStyle:{
        color: titleColor,
        fontSize: 16,
        paddingHorizontal: 5,
        fontWeight: '700'
    }


});


export {InputMask};