import React, {useState} from 'react';
import {
    View, Text, Modal, TouchableOpacity, StyleSheet, Keyboard
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {Button} from "../../components/common/Button";

import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import {dialogText} from "../../components/common/styles/geralStyle";

import {
    blackBackground, buttonsContainer, buttonsWrapper,
    cancelButtonStyle, iconStyle, titleWrapper, whiteContainer,
    whiteTitleStyle
} from "../../components/common/styles/dialogStyles";
import {Input} from "../../components/common/Input";
import {filterUpdateAction, setFilterAction, stateChangedAction} from "../../redux/actions";

import {InputLogin} from "../../components/common/InputLogin";
import {fixedType, fixedTypeItems} from "../../components/common/constants/index";
import DialogSubTitle from "../../components/common/DialogSubTitle";


const AddCategoryDialog = (props) => {
    const {saving, dissmissDialog, visible,  onPressConfirm, onPressCancel, title} =props ||{}
    const [name, setName] = useState('')


    return (

        <Modal
            onRequestClose={() => {
            }}
            animationType='slide' transparent visible={visible}>
            <View style={blackBackground}>
                <View style={whiteContainer}>

                    <View style={titleWrapper}>

                        <Text style={whiteTitleStyle}> {title} </Text>

                    </View>


                    <View>
                        <View style={{padding: 10}}>


                            <View>
                                <DialogSubTitle label={'Nome Da Categoria: '}/>
                                <View style={{flexDirection: 'row', width: '100%',}}>

                                    <InputLogin
                                        borderColor={purpleTitle}
                                        value={name}
                                        placeholder={'Digite a categoria'}
                                        setRef={(input) => this.passwordRef = input}
                                        onSubmitEditing={() => {
                                            Keyboard.dismiss()
                                        }}
                                        onChangeText={value => {
                                            setName(value)
                                        }}
                                    />
                                </View>
                            </View>


                            <View
                                style={buttonsContainer}
                            >
                                <View style={buttonsWrapper}>
                                    {onPressCancel &&
                                    <TouchableOpacity
                                        onPress={onPressCancel}
                                    >
                                        <Text style={cancelButtonStyle}>FECHAR</Text>
                                    </TouchableOpacity>

                                    }


                                    <Button
                                        color={purpleTitle}
                                        disabled={saving}
                                        onPress={()=>onPressConfirm(name)}
                                        label={saving ? 'Salvando' : 'Adicionar'}/>


                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </Modal>
    )


}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        width: 400,
        paddingVertical: 10,
        fontWeight: '700',
        paddingHorizontal: 10,
        borderRadius: 4,
        color: titleColor,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        fontWeight: '700',
        paddingHorizontal: 10,
        width: 400,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: purpleTitle,


        borderRadius: 8,
        color: titleColor,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});



export default AddCategoryDialog






