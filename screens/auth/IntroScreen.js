import React, {useEffect} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Dimensions,
    Picker,
    ScrollView,
    RefreshControl,
    ActivityIndicator,
    TouchableOpacity
} from 'react-native';

import {connect} from 'react-redux'
import {checkLoggedAction} from '../../redux/actions'
import {messageStyle, seeMoreContainer} from "../../components/common/styles/geralStyle";
import {gradientColor, purpleTitle, titleGrey} from "../../components/common/colors/index";
import {TransparentHeader} from "../../components/common/TransparentHeader";
import {titleStyle} from "../../components/common/styles/loginSignupStyle";
import {StackActions, NavigationActions} from 'react-navigation';

import {InputLogin} from "../../components/common/InputLogin";
import {Button} from "../../components/common/Button";
import {Card,} from 'react-native-paper';


const {height, width} = Dimensions.get('window')

const IntroScreen = (props) => {
    const {checkLoggedAction, navigation} = props || {}


    useEffect(() => {
        checkLoggedAction(callback)
    }, [])


    callback = () => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'main'})],
        });
        navigation.dispatch(resetAction);
    }


    const {container, content, title, section, pickerWrapper, pickkerText, picker} = styles


    return (
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={{position: 'absolute', width: '100%', height: '100%'}}>

                <View
                    style={{backgroundColor: '#FFFFFF98', position: 'absolute', width: '100%', height: '100%'}}></View>
            </View>

            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={false}
                        onRefresh={null}
                    />
                }>
                <View style={{flex: 1}}>
                    <View style={content}>
                        <Text>Sua loja aqui</Text>

                    </View>

                    {/*   <View style={styles.loading} >
                        <ActivityIndicator
                            color={purpleTitle}
                            size='large'/>


                    </View>*/}
                </View>
            </ScrollView>


        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: purpleTitle
    },
    content: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        marginTop: '30%',
        paddingHorizontal: 20
    },
    title: {
        fontWeight: '700', fontSize: 16,
    },

    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}
});


const mapStateToProps = (state) => {

    return {
        auth: state.auth.auth,
        errorMessage: state.auth.errorMessage,
        checked: state.auth.checked

    }
}


export default connect(mapStateToProps, {checkLoggedAction})(IntroScreen)



