export const  facebookBlue =  '#3b5998'
export const  logoBlue =   '#114C6C'
export const  lightPurple =   '#ffdd00'
export const  purpleTitle =   '#f6511d'
export const  darkPurple =   '#d77135'
export const    grey= "#eae5e5"
export const    titleGrey= "#9EA0A4"
export const    darkGrey= "#454A58"
export const    titleColor= "#1F1E24"
export const  gradientColor =   [purpleTitle, purpleTitle, purpleTitle];

 export  const background_color = {
     type_bug: "#8BD674",
     type_dark: "#6F6E78",
     type_dragon : "#7383B9",
     type_electric : "#F2CB55",
     type_fairy : "#EBA8C3",
     type_fighting : "#EB4971",
     type_fire : "#FFA756",
     type_flying : "#83A2E3",
     type_ghost : "#8571BE",
     type_ground : "#F78551",
     type_ice : "#91D8DF",
     type_normal : "#B5B9C4",
     type_poison : "#9F6E97",
     type_pshychic : "#FF6568",
     type_rock : "#D4C294",
     steel: '#4C91B2',
     water : '#58ABF6',
     grass : '#8BBE8A',
}


export  const type_color = {
    type_bug: "#8CB230",
    type_dark: "#58575F",
    type_dragon : "#0F6AC0",
    type_electric : "#EED535",
    type_fairy : "#ED6EC7",
    type_fighting : "#D04164",
    type_fire : "#FD7D24",
    type_flying : "#748FC9",
    type_ghost : "#556AAE",
    type_ground : "#DD7748",
    type_ice : "#61CEC0",
    type_normal : "#9DA0AA",
    type_poison : "#A552CC",
    type_pshychic : "#EA5D60",
    type_rock : "#BAAB82",
    steel: '#417D9A',
    water : '#4A90DA',
    grass : '#62B957',
}

