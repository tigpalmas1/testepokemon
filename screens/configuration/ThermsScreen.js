import React from 'react';
import {StyleSheet, Text, View,   WebView, ActivityIndicator } from 'react-native';
import {Header} from "../../components/Header";
import {purpleTitle} from "../../components/common/colors/index";


class ThermsScreen extends React.Component {
    static navigationOptions = {header: null}

    constructor(props) {
        super(props)
        this.state = { visible: true };


    }



    hideSpinner() {
        this.setState({ visible: false });
    }



    render() {

        const {title, url} = this.props.navigation.state.params


        return (


            <View style={styles.loading}>

                <Header
                    backButton={true}
                    onPress={() => {
                        this.props.navigation.goBack(null)
                    }}
                    title={title}/>

                <WebView
                    onLoadEnd = {() => this.hideSpinner()}
                    source={{uri: 'https://vendedorsolar.com.br/politicas.html'}}
                    style={{margin: 2, flex: 1}}
                />

                {this.state.visible && (
                    <ActivityIndicator
                        color={purpleTitle}
                        style={{ position: "absolute", top:'50%', left:'50%' }}
                        size="large"
                    />
                )}


            </View>


        )
    }
}

const styles = StyleSheet.create({
    loading: {
        position: 'absolute', width: '100%',
        height: '100%',
        borderRadius: 15,
        right: 0,
        backgroundColor: 'white'
    },

});



export default ThermsScreen;


