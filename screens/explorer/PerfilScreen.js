import React, {useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,

} from 'react-native';

import {connect} from "react-redux";
import {Header} from "../../components/Header";
import {logoutAction, instalerUpdateAction} from "../../redux/actions";

import {titleColor} from "../../components/common/colors/index";

import {StackActions, NavigationActions} from 'react-navigation';
import {PerfilButton} from "../../components/common/PerfilButton";
import {ConfirmationDialog} from "../dialogs/ConfirmationDialog";
import {EBTExt} from "../../components/EBText";
import NotLoggedFragment from '../fragment/NotLoggedFragment';

const PerfilScreen = (props) => {
    const {user, permission, navigation, logged} = props;
    const {logoutAction} = props;
    const {name, email} = user || {};
    const {isInstaller, perfilPlus} = permission || {};


    const [confirmationDialogVisible, isconfirmationDialogVisible] = useState(false);




    const renderView = () => {
        return (
            <View style={{flex: 1}}>
                <ScrollView
                    style={{flex: 1}}
                    scrollEventThrottle={16}>
                    <View style={{flex: 1}}>

                        <View style={{paddingHorizontal: 20,}}>


                            <View style={{flexDirection: 'row', alignItems: 'center'}}>


                                <View style={{marginLeft: 10}}>
                                    <Text style={{fontSize: 18, fontWeight: '700', color: 'grey'}}>{name}</Text>
                                    <Text style={{fontSize: 18, fontWeight: '700', color: 'grey'}}>{email}</Text>
                                </View>

                            </View>


                            <View>
                                <View style={{marginTop: 20}}><EBTExt
                                    style={{fontSize: 18, fontWeight: '700', color: titleColor}}>Informações</EBTExt>
                                </View>
                                <View style={{marginTop: 5, paddingHorizontal: 10}}>
                                    <PerfilButton
                                        iconName={'history'}
                                        label={'Endereço de Entrega'}
                                        onPress={() => navigation.navigate('addressList')}/>

                                    <PerfilButton
                                        iconName={'history'}
                                        label={'Meios de pagamento'}
                                        onPress={() => navigation.navigate('budget')}/>


                                    <PerfilButton
                                        iconName={'image-edit'}
                                        label={'Gerenciar Produtos'}
                                        onPress={() => navigation.navigate('productsOwner')}/>


                                </View>
                            </View>




                            <View style={{marginTop: 10}}><EBTExt
                                style={{fontSize: 18, fontWeight: '700', color: titleColor}}>Outros</EBTExt></View>
                            <View style={{marginTop: 5, paddingHorizontal: 10, marginBottom: 20}}>
                                <PerfilButton
                                    iconName={'information-variant'}
                                    label={'Alterar Senha'}
                                    onPress={() => navigation.navigate('changePassword')}/>


                                <PerfilButton
                                    iconName={'logout'}
                                    label={'Encerrar Sessão'}
                                    onPress={() => logoutAction()}/>


                            </View>
                        </View>


                    </View>


                    <ConfirmationDialog
                        cancelButton
                        visible={confirmationDialogVisible}
                        onPressConfirm={() => this.cancelOrderPress()}
                        onPressCancel={() => isconfirmationDialogVisible(false)}
                        title="Tornar-se um Instalador Premiu"
                        text={`Sua solicitação será enviada para um de nosso representantes, aguarde retorno`}/>


                </ScrollView>


            </View>

        )


    };


    if(logged){
        return (



            <View style={styles.container}>
                <Header
                    colorText={titleColor}
                    title={ 'Perfil'}
                />



                {renderView()}


            </View>

        );
    }else{
        return <NotLoggedFragment

            onLoginPress={() => navigation.navigate('login')}
            onSignupPress={() => navigation.navigate('signup')}
            hasButton
            message={'Não autenticado'}

        />
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",


    },
    header: {
        marginTop: 20,
        height: 200,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },

});

const mapStateToProps = (state) => {


    return {
        logged: state.auth.logged,
        auth: state.auth.auth,
        user: state.user.user,
        permission: state.user.permission,
    }
};


export default connect(mapStateToProps, {logoutAction, instalerUpdateAction})(PerfilScreen)

