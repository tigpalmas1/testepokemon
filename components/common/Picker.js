import React, {Component} from 'react';
import {
    View, Text, TouchableOpacity
} from 'react-native';
import {tag, tagWrapper} from "./styles/geralStyle";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";


const Picker = ({typeUpdate, type, typeError}) => {
    const {pickerContainer} = styles;

    return (

        <View style={{marginTop:0}}>

            <Text style={{
                color: typeError? 'red': titleGrey,
                marginBottom: 5,
                fontSize: 16,
                fontWeight: '700',
            }}>{'Tipo de usuário'}</Text>
            <View style={pickerContainer}>
                <TouchableOpacity onPress={() => typeUpdate('customer')}>
                    <View
                        style={[tagWrapper, {backgroundColor: type === 'customer' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Consumidor</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => typeUpdate('installer')}>
                    <View
                        style={[tagWrapper, {backgroundColor: type === 'installer' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Instalador</Text>
                    </View>

                </TouchableOpacity>

                <TouchableOpacity onPress={() => typeUpdate('seller')}>
                    <View
                        style={[tagWrapper, {backgroundColor: type === 'seller' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Vendedor</Text>
                    </View>

                </TouchableOpacity>


            </View>
            <Text style={{color: 'red', fontWeight: '700', fontSize: 8}}>{typeError}</Text>
        </View>


    )
}

const styles = {
    pickerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    }
}

export {Picker};

