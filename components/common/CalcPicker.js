import React, {Component} from 'react';
import {
    View, Text, TouchableOpacity
} from 'react-native';
import {tag, tagWrapper} from "./styles/geralStyle";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";
import DialogSubTitle from "./DialogSubTitle";


const CalcPicker = ({typeUpdate, type, typeError}) => {
    const {pickerContainer} = styles;

    return (

        <View style={{marginTop:0}}>

           <DialogSubTitle label={'Calcular por:'} error={typeError}/>
            <View style={pickerContainer}>
                <TouchableOpacity onPress={() => typeUpdate('kWh')}>
                    <View
                        style={[tagWrapper, {backgroundColor: type === 'kWh' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>kWh</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => typeUpdate('kWp')}>
                    <View
                        style={[tagWrapper, {backgroundColor: type === 'kWp' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>kWp</Text>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => typeUpdate('modulesNumber')}>
                    <View
                        style={[tagWrapper, {backgroundColor: type === 'modulesNumber' ? purpleTitle : titleGrey}]}>
                        <Text style={tag}>Número de Módulos</Text>
                    </View>

                </TouchableOpacity>

            </View>
            <Text style={{color: 'red', fontWeight: '700', fontSize: 8}}>{typeError}</Text>
        </View>


    )
}

const styles = {
    pickerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    }
}

export {CalcPicker};

