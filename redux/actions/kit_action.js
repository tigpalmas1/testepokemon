import {
    ADD_ITEM,
    CLEAR_KITS_LIST,
    ESTABLISHMENT_COORDENATES_UPDATE, FETCH_KITS, FETCH_KITS_LOADING, FETCH_KITS_PROMOIONAL_LOADING,
    FETCH_KITS_PROMOTIONAL, REMOVE_ITEM, SET_KIT,

} from "./types";
import {checkResponseError, getStorage} from "../../components/common/functions/index";
import axios from 'axios'
import {baseUrl} from "../../components/common/constants/index";

export const clearKItsList = () => async (dispatch) => {
    dispatch({type: CLEAR_KITS_LIST})
};

export const setKit = (kit) => async (dispatch) => {
    dispatch({type: SET_KIT, payload: kit})
};

export const addItem = (objectId) => async (dispatch) => {
    dispatch({type: ADD_ITEM, payload: objectId})
};
export const removeItem = (objectId) => async (dispatch) => {
    dispatch({type: REMOVE_ITEM, payload: objectId})
};

export const fetchKits = (skip, filter) => async (dispatch) => {


    dispatch({type: FETCH_KITS_LOADING, payload: {loading: true, message: 'Buscando Kits'}});


    try {
        let url = `${baseUrl}/kit?`;
        let config = '';
        const {city, value, state,orderBy, fixedType,searchType, code }= filter ||{};


        if (filter)
            config = {
                params: {
                    code: code,
                    city: city,
                    state: state,
                    fixedType: fixedType,
                    searchType: searchType,
                    value: value,
                    orderBy: orderBy,
                    skip: skip,
                    limit: 5

                },
            };
        const {data} = await axios.get(url, config);
        console.log(data)
        if(data.length>0){
            data.push({
                _id: skip,
                advertising: true
            })
        }



        dispatch({
            type: FETCH_KITS,
            payload: {
                kits: data,
                message: data.length > 0 ? '' : 'Sem kits para essa pesquisa',
                endList: data.length < 5,
            }
        });

    } catch (e) {
        dispatch({type: FETCH_KITS_LOADING, payload: {loading: false, message: checkResponseError(e)}});

    }
};





