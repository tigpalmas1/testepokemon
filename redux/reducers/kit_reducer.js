import {

    FETCH_KITS_LOADING, FETCH_KITS, LOGOUT, CLEAR_KITS_LIST, SET_KIT, ADD_ITEM, REMOVE_ITEM,
    FETCH_KITS_PROMOIONAL_LOADING, FETCH_KITS_PROMOTIONAL,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    loading: false,
    kits: [],
    message: '',
    endList: false,
    kit:{
        itemId:[]
    },
    messagePromotional: '',
    loadingPromotional: false,
    promotionalKits:[],
    promotionalEndList: false,

}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {
        case SET_KIT:
            return {
                ...state,
                kit: payload
            }

        case FETCH_KITS_LOADING:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading
            }
        case FETCH_KITS_PROMOIONAL_LOADING:
            return {
                ...state,
                messagePromotional: payload.messagePromotional,
                loadingPromotional: payload.loadingPromotional
            }
        case ADD_ITEM:
            return {
                ...state,
                kit:{
                    ...state.kit,
                    itemId: state.kit.itemId.map(
                        (item, i) => item._id === payload ? {...item, quantity: item.quantity+1}: item
                    )
                }
            }
        case REMOVE_ITEM:
            return {
                ...state,
                kit:{
                    ...state.kit,
                    itemId: state.kit.itemId.map(
                        (item, i) => item._id === payload ? {...item, quantity: item.quantity-1}: item
                    )
                }
            }

        case FETCH_KITS:
            return {
                ...state,
                kits: _.uniqBy(state.kits.concat(payload.kits), "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            }

        case FETCH_KITS_PROMOTIONAL:
            return {
                ...state,
                promotionalKits: _.uniqBy(state.promotionalKits.concat(payload.promotionalKits), "_id"),
                loadingPromotional: false,
                messagePromotional: payload.messagePromotional,
                promotionalEndList:payload.promotionalEndList

            }
        case CLEAR_KITS_LIST:
            return {
                ...state,
                kits: [],
                loading: false,
                message: ''
            }

        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

