import {

    FETCH_KITS_LOADING, FETCH_KITS, LOGOUT, CLEAR_KITS_LIST, SET_KIT, ADD_ITEM, REMOVE_ITEM, FETCH_ORDERS_LOADING,
    FETCH_ORDERS, SET_SELECTED_INSTALER, CLEAR_SELECTED_INSTALER,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    loading: false,
    orders: [],
    message: '',
    selectedInstaler: null,
    endList: false

}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {


        case FETCH_ORDERS_LOADING:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading
            }

        case FETCH_ORDERS:
            return {
                ...state,
                orders: _.uniqBy(payload.orders, "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList

            }

        case SET_SELECTED_INSTALER:
            return {
                ...state,
                selectedInstaler: payload
            }

        case CLEAR_SELECTED_INSTALER:
            return {
                ...state,
                selectedInstaler: null
            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

