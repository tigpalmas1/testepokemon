import React,{useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    Share,
    Alert,
    Platform
} from 'react-native';


import {grey, lightPurple, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";

import {Button} from "../../components/common/Button";
import {connect} from "react-redux";
import {ProductItem} from '../kits/ProductItem'

import {
    colorShimer,
    regularText, seeMoreStyle, textSeeMoreStyle,
    vipListContainer
} from "../../components/common/styles/geralStyle";
import {Header} from "../../components/Header";
import {saveOrderAction, addItem, removeItem, clearSelectedInstaler} from "../../redux/actions/index";
import {ConfirmationDialog} from "../dialogs/ConfirmationDialog";
import {formataDinheiro, getNationalValue} from "../../components/common/functions/index";
import {TagButton} from "../../components/common/TagButton";
import {TransparentHeader} from "../../components/common/TransparentHeader";
import {EBTExt} from "../../components/EBText";
import {RBText} from "../../components/RBText";
import {background_color} from "../../components/common/colors/index";
import {ButtonPerfilItem} from "../../components/common/ButtonPerfilItem";

const {height, width} = Dimensions.get('window');

export default ProductDetailScreen = (props) => {
    const {navigation} = props || {}
    const [activeIndex, setActiveIndex] = useState(0);

    const {product} = navigation.state.params || {};


    const {name, type, img, num} = product || {}

    const getColor = () => {
        switch (type[0]) {
            case  'Dark':
                return background_color.type_dark
            case  'Bug':
                return background_color.type_bug
            case  'Grass':
                return background_color.grass
            case  'Dragon':
                return background_color.type_dragon
            case  'Electric':
                return background_color.type_electric
            case  'Fairy':
                return background_color.type_fairy
            case  'Flying':
                return background_color.type_flying
            case  'Fire':
                return background_color.type_fire
            case 'Water':
                return background_color.water
            case  'Ghost':
                return background_color.type_ghost
            case  'Ground':
                return background_color.type_ground
            case  'Ice':
                return background_color.type_ice
            case  'Normal':
                return background_color.type_normal
            case  'Poison':
                return background_color.type_poison
            case  'Psychic':
                return background_color.type_pshychic
            case  'Rock':
                return background_color.type_rock
            case  'Steel':
                return background_color.steel

            default:

                return null
        }

    }


    const {
        container,
        dateText, textTitle, scrollContainer, lineStyle, timeStyle,
        titleWrapper, beginTimeInfo, avatarImg, eventImgContainer, avatarImageContainer, avatarImageContainerIos
    } = styles;

    const segmenteClicke = (index) => {
        setActiveIndex(index);
    };

    const renderSection = () => {
        if (activeIndex === 0) {
            return (
               <View>

               </View>
            )
        }


    };

    return (

        <View style={container}>
            <ScrollView


            >


                <View style={{
                    height: height * 0.45, width: '100%', backgroundColor: getColor()
                }}>
                    <View style={{
                        height: 200, marginTop: 50,
                        width: '100%', flexDirection: 'row', alignItems: 'center', padding: 20
                    }}>

                        <View style={{flex: 5,}}>
                            < Image
                                reziseMode={'contain'}
                                style={{
                                    flex: 1, width: null,
                                }}
                                source={{uri: img}}/>
                        </View>
                        <View style={{flex: 5, height: 50}}>
                            <Text style={{fontWeight: '700'}}>#{num}</Text>
                            <Text style={{fontSize: 28, color: 'white', fontWeight: '700'}}>{name}</Text>
                            <View style={{flexDirection: 'row'}}>
                                {type.map(el => (
                                    <TagButton title={el}/>
                                ))}
                            </View>

                        </View>

                    </View>

                    <View style={{
                        flexDirection: 'row',
                        width: '100%',
                        background_color: 'pink',
                        alignItems: 'center',
                        justifyContent: 'space-around'
                    }}>
                        <ButtonPerfilItem
                            index={0}
                            segmenteClick={() => segmenteClicke(0)}
                            activeIndex={activeIndex}
                             fontSize={16} name="About" textColor="white"/>
                        <ButtonPerfilItem
                            segmenteClick={() => segmenteClicke(1)}
                            activeIndex={activeIndex}
                             fontSize={16} name="Stats" textColor="white"/>
                        <ButtonPerfilItem
                            segmenteClick={() => segmenteClicke(2)}
                            activeIndex={activeIndex}
                             fontSize={16} name="Evolution" textColor="white"/>


                    </View>


                </View>


                <View
                    style={{
                        flex: 1,
                        padding: 25,
                        backgroundColor: 'white',
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        marginTop: -20
                    }}>


                    <Text style={{fontSize: 18, color: 'grey'}}>
                        Bulbasaur can be seen napping in bright sunlight. There is a seed on its back. By soaking up the sun's rays. the seed grows progressively larger.
                    </Text>

                    <Text style={{fontSize: 18, color: background_color.grass, fontWeight: '700', marginVertical:20}}>
                        Pokédex Data
                    </Text>

                    <View style={{flexDirection: 'row'}}>
                        <View style={{marginRight:20}}>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Species</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Height</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Weigth</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Abilities</Text>

                        </View>

                        <View>
                            <Text style={{marginBottom: 10, fontSize: 16, color: 'grey'}}>Seed Pokémon</Text>
                            <Text style={{marginBottom: 10, fontSize: 16, color: 'grey'}}>0.7m(2,04)</Text>
                            <Text style={{marginBottom: 10, fontSize: 16, color: 'grey'}}>6.9kg(15.2lbs)</Text>
                            <Text style={{ fontSize: 16, color: 'grey'}}>1.Overgrow</Text>
                        </View>

                    </View>

                    <Text style={{fontSize: 18, color: background_color.grass, fontWeight: '700', marginVertical:20}}>
                       Training
                    </Text>


                    <View style={{flexDirection: 'row'}}>
                        <View style={{marginRight:20}}>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Ev Yield</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Catch Rate</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Base Friendshipg</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Base Exp</Text>
                            <Text style={{marginBottom: 10, fontSize: 16}}>Grow Slow</Text>

                        </View>

                        <View>
                            <Text style={{marginBottom: 10, fontSize: 16, color: 'grey'}}>1 Special Super Attack</Text>
                            <Text style={{marginBottom: 10, fontSize: 16, color: 'grey'}}>45 (5.9% with pokeball</Text>
                            <Text style={{marginBottom: 10, fontSize: 16, color: 'grey'}}>64</Text>
                            <Text style={{ fontSize: 16, color: 'grey'}}>1.Overgrow</Text>
                            <Text style={{ fontSize: 16, color: 'grey'}}>Mediu Slow</Text>
                        </View>

                    </View>


                </View>


            </ScrollView>


            <View style={{position: 'absolute', top: 0, right: 0, left: 0}}>
                <TransparentHeader
                    colorText={titleColor}
                    backButton
                    onPress={() => {
                        navigation.goBack(null)
                    }}
                    title={""}
                />
            </View>


        </View>


    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    beginTimeInfo: {color: 'grey', fontSize: 14, marginLeft: 5, marginRight: 10},


    eventImg: {
        flex: 1,
        height: '100%',
        width: '100%',

    },

    avatarImageContainer: {
        position: 'absolute',
        right: 20,
        zIndex: 1,
        bottom: -30,


    },

    avatarImageContainerIos: {
        position: 'absolute',
        right: 20,
        zIndex: 1,
        top: -30,

        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },


    avatarImg: {
        width: 60,
        height: 60,
        borderRadius: 30,

        borderWidth: 1,
        borderColor: 'white'
    },
    titleWrapper: {
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',

    },
    inputWrapper: {
        flexDirection: 'row',
        marginHorizontal: 10,
        backgroundColor: 'white',
        marginTop: 10,
        padding: 10,
    }, dateText: {fontSize: 22, color: purpleTitle, fontWeight: '900', lineHeight: 24},
    textTitle: {fontSize: 32, fontWeight: '900', lineHeight: 31, color: titleColor},
    scrollContainer: {flex: 1, height: '100%', width: '100%',},
    lineStyle: {width: '100%', marginTop: 5, borderBottomWidth: 1, borderColor: 'grey',},
    timeStyle: {width: '100%', padding: 5, alignItems: 'center', flexDirection: 'row',}


});




