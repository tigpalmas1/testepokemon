import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


import {Card,} from 'react-native-paper';
import {
    addCartItemAction
} from "../../redux/actions";
import {useDispatch, useSelector} from 'react-redux';

const WIDTH = Dimensions.get('window').width;


export default CartItem = (props) => {
    const {onPress, product} = props || {};
    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart.cart);

    const {imageUrl, name, value} = product || {}

    return (
        <Card style={{
            elevation: 11,
            justifyContent: 'center',
            padding: 10,
            width: '100%',
            margin: 2,
            flexDirection: 'row',
        }}>

            <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>

                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                        reziseMode={'resize'}
                        style={{
                            width: 80, height: 80, borderRadius: 40,
                        }}
                        source={{uri: imageUrl}}/>


                    <View style={{marginLeft: 10}}>
                        <Text style={{fontWeight: '700', fontSize: 18, color: titleColor}}>{name}</Text>

                    </View>

                </View>


                <View style={{marginLeft: 10}}>
                    <Text style={{fontWeight: '700', fontSize: 18, color: 'grey'}}>2x</Text>

                </View>


            </View>


        </Card>





    )


}

const styles = StyleSheet.create({});


