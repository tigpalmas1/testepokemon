import React, {Component} from 'react';
import {
    View, Text, Modal, TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {connect} from 'react-redux'


import {Button} from "../../components/common/Button";

import {grey, purpleTitle, titleColor} from "../../components/common/colors/index";
import {dialogText} from "../../components/common/styles/geralStyle";

import {
    blackBackground, buttonsContainer, buttonsWrapper,
    cancelButtonStyle, iconStyle, titleWrapper, whiteContainer,
    whiteTitleStyle
} from "../../components/common/styles/dialogStyles";
import {Input} from "../../components/common/Input";
import {categoryUpdate, addCategoryAction} from "../../redux/actions";
import {intalerTherms} from "../../components/common/constants/index";
import {CheckBox} from "../../components/common/CheckBox";


class InstalerThermsDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saving: false,
            nameError: '',
            acceptTherms: false ,
            thermsError: '',

        }
    }


    onPressButton = () => {


        if (!this.checkError()) {
                   this.props.dismissDialog()
        }
    }

    saveCallback = () => {
        this.setState({saving: false, })
        categoryUpdate({prop: 'name', value: ''})
        this.props.onCoverDismiss()
    }


    checkError() {
        const {acceptTherms,} = this.state || {}
        let errors = false;
        if (!acceptTherms) {
            this.setState({thermsError: 'Você deve aceitar os thermos para continuar'})
            errors = true
        }


        return errors

    }


    render() {
        const {nameError, saving, acceptTherms, thermsError} = this.state
        const {
            visible, title, text, onPositivePress,
            loading,
            cancelButton,
            loadingText,
            onCoverDismiss,
            iconName, category, categoryUpdate, action
        } = this.props;

        const {name} = category || {}




        return (

            <Modal
                onRequestClose={() => {
                }}
                animationType='slide' transparent visible={visible}>
                <View style={blackBackground}>
                    <View style={whiteContainer}>


                        <View style={titleWrapper}>
                            <Icon
                                size={20}
                                name={iconName}
                                style={iconStyle}
                            />
                            <Text style={whiteTitleStyle}> {title} </Text>

                        </View>


                        <View>
                            <View style={{padding: 10}}>


                                  <Text style={{fontSize:20, color: titleColor, fontWeight:'700'}}>{intalerTherms}</Text>




                                <View
                                    style={buttonsContainer}
                                >
                                    <View style={buttonsWrapper}>


                                        <CheckBox
                                            error={thermsError}
                                            checked={acceptTherms}
                                            onPressCheck={()=>{
                                                this.setState({acceptTherms: !acceptTherms})

                                            }}
                                            label={'Estou ciente e quero continuar'}/>


                                        {cancelButton &&
                                        <TouchableOpacity
                                            onPress={onCoverDismiss}
                                        >
                                            <Text style={cancelButtonStyle}>CANCELAR</Text>
                                        </TouchableOpacity>

                                        }



                                        <Button
                                            color={purpleTitle}
                                            disabled={saving}
                                            onPress={this.onPressButton}
                                            label={saving? 'Salvando': 'Confirmar'}/>


                                    </View>


                                </View>
                            </View>


                        </View>


                    </View>
                </View>
            </Modal>
        )
    }


}

const styles = {
    loading: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'

    },
    tag: {
        fontSize: 10,
        color: 'white',
        fontWeight: '700'


    },

    tagWrapper: {
        padding: 5,

        backgroundColor: purpleTitle,
        flexWrap: 'wrap',
        borderRadius: 12,
        marginRight: 5
    },

    tagGreyWrapper: {
        padding: 5,
        backgroundColor: grey,
        flexWrap: 'wrap',
        borderRadius: 12,
        marginRight: 5
    }


}

const mapStateToProps = (state) => {


    return {


    };
};
export default connect(mapStateToProps, {categoryUpdate, addCategoryAction})(InstalerThermsDialog)






