import React from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Dimensions,
    Picker,
    ScrollView,
    RefreshControl
} from 'react-native';

import {connect} from 'react-redux'
import {authUpdate, passwordResendAction, validationEmailResendAction} from "../../redux/actions";
import {darkGrey, facebookBlue, logoBlue, purpleTitle, titleGrey} from "../../components/common/colors/index";
import {titleStyle} from "../../components/common/styles/loginSignupStyle";
import {PASSWORD_RECOVER, RESEND_VALIDATION_EMAIL} from "../../components/common/constants/index";
import {InputLogin} from "../../components/common/InputLogin";
import {Button} from "../../components/common/Button";
import {TransparentHeader} from "../../components/common/TransparentHeader";
import {Card,} from 'react-native-paper';


const {height, width} = Dimensions.get('window')

class PasswordRecoverScreen extends React.Component {
    static navigationOptions = {header: null}


    constructor(props) {
        super(props)
        this.state = {
            emailError: '',
            recoverLoading: false,
            errorMessage: '',
        };
    }


    componentDidUpdate() {
        if (this.props.logged) {
            this.props.navigation.goBack(null)
        }

    }

    onLoginPress = () => {
        const { action} = this.props.navigation.state.params || {}

        if (!this.checkError()) {
            this.setState({recoverLoading: true})
            if(action === PASSWORD_RECOVER){
                this.props.passwordResendAction(this.props.auth.email, this.callbackReset)
            }else if(action === RESEND_VALIDATION_EMAIL){
                this.props.validationEmailResendAction( this.callbackReset)
            }
        }
    }

    callbackReset = (message) =>{
        this.setState({
            recoverLoading: false
        });
        Alert.alert(message);
        this.props.navigation.goBack(null)
    }


    checkError() {
        const {email} = this.props.auth || {}
        let errors = false;

        if (!email) {
            this.setState({emailError: 'Email obrigátorio'})
            errors = true
        }
        if (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            this.setState({emailError: 'Email está no formato incorreto'})
            errors = true
        }


        return errors

    }

    render() {
        const {container, content,  section, pickerWrapper, pickkerText, picker} = styles
        const {emailError, recoverLoading, errorMessage} = this.state
        const {email, password} = this.props.auth
        const {title, action} = this.props.navigation.state.params || {}
        return (
            <View style={{flex: 1, backgroundColor: purpleTitle}}>

                <View>

                    <TransparentHeader

                        arrowColor={'black'}
                        backButton
                        onPress={() => {
                            this.props.navigation.goBack(null)
                        }}
                        title={'Recuperar Senha'}
                    />
                </View>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={recoverLoading}
                            onRefresh={null}
                        />
                    }>
                    <View style={content}>
                        <Text style={titleStyle}>{title}</Text>
                        <View style={{flex: 1, marginTop: 60,}}>


                            <Card style={{elevation: 11, width: '100%', marginTop: 20, paddingHorizontal: 10, paddingVertical: 30}}>
                            <View style={{flexDirection: 'row', width: '100%', marginTop: 20}}>

                                <InputLogin
                                    icon={"email"}
                                    label={'Email'}
                                    autoCapitalize={'none'}
                                    error={emailError}
                                    placeholder={'seuemail@gmail.com'}
                                    value={email}
                                    onChangeText={value => {
                                        this.setState({emailError: ''})
                                        this.props.authUpdate({prop: 'email', value: value.trim()})}}
                                />
                            </View>



                            <View style={{width: '100%', alignItems: 'center', marginTop: 20}}>
                                <Text style={{color: 'red', fontWeight: '700'}}>{errorMessage}</Text>
                            </View>

                            </Card>



                            <View style={{marginTop: 40}}>

                                <Button
                                    color={logoBlue}
                                    disabled={recoverLoading}
                                    onPress={this.onLoginPress}
                                    label={recoverLoading? 'Enviando senha...': 'Enviar'}/>
                            </View>





                        </View>
                    </View>
                </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: purpleTitle
    },
    content: {
        flex: 1,
        paddingHorizontal: 20
    },


    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}
});


const mapStateToProps = (state) => {

    return {
        auth: state.auth.auth,

    }
}


export default connect(mapStateToProps, {
    passwordResendAction,
    validationEmailResendAction,
    authUpdate
})(PasswordRecoverScreen)



