import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';


import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {purpleTitle, titleColor, titleGrey} from "./colors/index";
import {txtCountText, txtCountWrapper} from "./styles/perfilStyle";


const ButtonPerfilItem = ({icon, name,  activeIndex, segmenteClick, iconName, index, fontSize, textColor}) => {


    return (
        <TouchableOpacity
            style={{flexWrap: 'wrap', }}
            onPress={segmenteClick}
            active={activeIndex}
            transparent>
            <View style={txtCountWrapper}>
                {iconName &&
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon
                        size={24}
                        style={[activeIndex === index ? {color: purpleTitle} : {color: 'grey'}]}
                        name={iconName}/>
                </View>
                }

                <Text style={[ {
                    color: activeIndex == index ? 'white' : textColor, fontWeight: '700',

                    fontSize: fontSize || 14}]}>{name}</Text>
                {activeIndex === index &&
                <View style={{width: 50, height: 1, backgroundColor: 'white',
                    color: activeIndex == index ? purpleTitle : textColor,
                }}></View>
                }

            </View>


        </TouchableOpacity>
    )
}

const styles = {}

export {ButtonPerfilItem};