import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from "./colors";
import {darkPurple, lightPurple, logoBlue, purpleTitle, titleGrey} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {LinearGradient} from 'expo';


const DialogSubTitle = ({label,  error}) => (
    <Text style={{
        color: error? 'red': titleGrey,
        marginBottom: 5,
        fontSize: 16,
        fontWeight: '700',
    }}>{label}</Text>
)

const styles = {
    textStyle: {
        alignSelf: 'center',

        fontSize: 18,
        paddingTop: 10,
        paddingBottom: 10,
        fontWeight: '900'
    },
    buttonStyle: {
        paddingHorizontal: 10,
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,

    }
}

export default DialogSubTitle;