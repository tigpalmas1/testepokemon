


//AUTH
export const AUTH_LOADING = 'auth_loading';
export const AUTH_UPDATE = 'auth_update';
export const IMAGE_USER_UPDATE = 'image_user_update';

export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_ERROR = 'login_error';
export const LOGIN_LOADING = 'login_loading';
export const SIGNUP_SUCCESS = 'signup_success';
export const SIGNUP_ERROR = 'signup_error';
export const SIGNUP_LOADING = 'signup_loading';



//INSTALER
export const FETCH_INSTALER_LOADING = 'fetch_instaler_loading';
export const FETCH_INSTALER = 'fetch_instaler';
export const INSTALER_UPDATE = 'instaler_update';
export const CARD_IMAGE_UPDATE = 'card_image_update';
export const SET_SELECTED_INSTALER = 'set_selected_instaler';
export const CLEAR_SELECTED_INSTALER = 'clear_selected_instaler';
export const CLEAR_INSTALER_LIST = 'clear_instaler_list';
export const SET_INSTALER = 'set_instaler';

//FILTER
export const FILTER_UPDATE = 'filter_update';
export const FILTER_UPDATE_TYPE = 'filter_update_type';
export const SET_FILTER = 'set_filter';
export const STATE_CHANGE_ACTION = 'state_change_action';

//KITS
export const FETCH_KITS = 'fetch_kits';
export const FETCH_KITS_LOADING = 'fetch_kits_loading';
export const CLEAR_KITS_LIST = 'clear_kits_list';
export const SET_KIT = 'set_main_kit';
export const ADD_ITEM = 'add_item';
export const REMOVE_ITEM = 'remove_item';


//PROMOTIONAL
export const FETCH_KITS_PROMOTIONAL = 'fetch_kits_promotional';
export const FETCH_KITS_PROMOIONAL_LOADING = 'fetch_kits_promotional_loading';




//ORDERS
export const FETCH_ORDERS = 'fetch_orders';
export const FETCH_ORDERS_LOADING = 'fetch_orders_loading';

//USER INFO


export const USER_INFO_UPDATE = 'user_info_update';
export const SET_UPDATE_USER = 'set_update_user';
export const CLEAR_USER = 'clear_user';
export const FETCH_USERS = 'fetch_users';
export const FETCH_USERS_LOADING = 'fetch_users_loading';
export const ADD_USER = 'add_user';
export const ADD_USER_LOADING = 'add_user_loading';
export const UPDATE_USER = 'update_user';
export const UPDATER_USER_LOCATION = 'user_location'


//ESTABLISHMENT
export const FETCH_ESTABLISHMENT = 'fetch_establishment';
export const ESTABLISHMENT_UPDATE = 'establishment_update';
export const ESTABLISHMENT_IMAGE_UPDATE = 'estalbihsment_image_update';
export const ESTABLISHMENT_LOADING = 'establishment_loading';
export const ESTABLISHMENT_SAVE_SUCCESS= 'establishment_save_sucess';
export const ESTABLISHMENT_SAVE_ERROR= 'establishment_save_error';


//TICKETS
export const FETCH_TICKET_LOADING = 'fetch_tickets_loading';
export const FETCH_TICKETS = 'fetch_tickets';
export const FETCH_TICKETS_ERROR = 'fetch_tickets_error';

export const FETCH_FAVORIT_TICKETS_LOADING = 'fetch_favorit_tickets_laoding';
export const FETCH_FAVORIT_TICKETS = 'fetch_favorit_tickets';
export const FETCH_FAVORIT_TICKETS_ERROR = 'fetch_favorit_tickets_error';

export const FETCH_ESTABLISHMENT_TICKETS = 'fetch_establishment_tickets';
export const FETCH_ACTIVE_TICKET = 'fetch_active_ticket';
export const TICKET_UPDATE = 'ticket_update'
export const IMAGE_UPDATE = 'image_update'
export const DATE_UPDATE = 'date_update'
export const TICKET_LOADING = 'ticket_loading'
export const TICKET_SAVE_SUCCESS = 'ticket_save_sucess'
export const TICKET_SAVE_ERROR = 'ticket_save_error'
export const CLEAR_FOUND_ITENS_LIST = 'clear_found_itens_list'

//PRODUCTS
export const SET_PRODUCT = 'set_product';
export const PRODUCT_UPDATE = 'product_update';
export const FETCH_PRODUCTS_LOADING = 'fetch_products_loading';
export const FETCH_PRODUCTS = 'fetch_products';
export const FETCH_DESTAQUE_PRODUCTS = 'fetch_destaque_products';
export const FETCH_PRODUCTS_ERROR = 'fetch_products_error';

//CATERGORIES
export const CATEGORY_UPDATE = 'category_update';
export const FETCH_CATEGORY_LOADING = 'fetch_categories_loading';
export const FETCH_CATEGORY = 'fetch_categories';
export const FETCH_CATEGORY_PRODUCTS = 'fetch_categories_products';
export const FETCH_CATEGORY_ERROR = 'fetch_category_error';


//STAMPS
export const FETCH_STAMPS = 'fetch_stamps';
export const REQUEST_STAMP = 'request_stamp';


//LOGOUT
export  const LOGOUT ='logout'

