import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {grey, logoBlue, purpleTitle, titleGrey} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {NBText} from "../NBText";



const InputLogin = ({label, error, value, onChangeText,
                        borderColor,
                        placeholder, secureTextEntry, icon, autoCapitalize,  setRef, onSubmitEditing, keyboardType,  maxLength}) => {

    const {inputStyle, labelStyle, containerStyle, inputWrapper, } = styles;

    return (
        <View style={containerStyle}>





            <View style={[inputWrapper, { borderColor: error? 'red': borderColor}]}>
                <Icon
                    size={24}
                    name={icon}
                    style={ {color: error? 'red':  '#9EA0A4', flexWrap:'wrap'} }
                />

                <TextInput
                    secureTextEntry={secureTextEntry}
                    autoCapitalize={autoCapitalize}
                    autoCorrect={false}
                    keyboardType={keyboardType}
                    maxLength={maxLength}
                    returnKeyType={'done'}
                    underlineColorAndroid='transparent'
                    placeholder={placeholder}
                    placeholderTextColor={'#9EA0A4'}
                    value={value}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                    ref={setRef}
                    blurOnSubmit={false}
                    style={inputStyle}/>
            </View>
            <NBText style={{color: 'red', fontWeight: '700', fontSize: 12}}>{error}</NBText>

        </View>
    )
}

const styles = {
    containerStyle: {
        flex: 1,


    },
    inputWrapper: {
        width: '100%',
        flexDirection: 'row',

        borderBottomWidth: 1,
        borderRadius: 10,

        padding: 10,

    },
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        flex: 1,

        color: 'black',
        fontSize: 16,
        fontWeight: '700'
    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',


    },

}

export {InputLogin};