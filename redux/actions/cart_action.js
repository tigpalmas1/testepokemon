import {
    ADD_ITEM,
    CLEAR_KITS_LIST,
    ESTABLISHMENT_COORDENATES_UPDATE, FETCH_KITS, FETCH_KITS_LOADING, FETCH_KITS_PROMOIONAL_LOADING,
    FETCH_KITS_PROMOTIONAL, REMOVE_ITEM, SET_KIT,

} from "./types";
import {checkResponseError, getStorage} from "../../components/common/functions/index";
import axios from 'axios'
import {baseUrl} from "../../components/common/constants/index";
import _ from 'lodash';

export const addCartItemAction = (cart, product) => async (dispatch) => {

    cartItem = {
        product,
        quantity: 1
    }



    dispatch({type: 'ADD_CART_ITEM', payload: cartItem})
};





