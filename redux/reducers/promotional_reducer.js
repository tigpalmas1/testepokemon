import {

    FETCH_KITS_LOADING, FETCH_KITS, LOGOUT, CLEAR_KITS_LIST, SET_KIT, ADD_ITEM, REMOVE_ITEM,
    FETCH_KITS_PROMOIONAL_LOADING, FETCH_KITS_PROMOTIONAL,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    message: '',
    loading: false,
    promotionalKits:[],
    endList: false,

}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {

        case FETCH_KITS_PROMOIONAL_LOADING:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading
            }

        case FETCH_KITS_PROMOTIONAL:
            return {
                ...state,
                kits: _.uniqBy(state.kits.concat(payload.kits), "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

