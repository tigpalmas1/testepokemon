import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Dimensions,
    Picker,
    Keyboard,
    ScrollView,
    RefreshControl,
    TouchableOpacity
} from 'react-native';

import {connect} from 'react-redux'
import {authUpdate, loginServerAction} from '../../redux/actions'
import {messageStyle, regularText, seeMoreContainer} from "../../components/common/styles/geralStyle";
import {logoBlue, purpleTitle, titleGrey} from "../../components/common/colors/index";
import {TransparentHeader} from "../../components/common/TransparentHeader";
import {titleStyle} from "../../components/common/styles/loginSignupStyle";
import {InputLogin} from "../../components/common/InputLogin";
import {Button} from "../../components/common/Button";
import {StackActions, NavigationActions} from 'react-navigation';

const {height, width} = Dimensions.get('window')

class WelcomeScreen extends React.Component {
    static navigationOptions = {header: null}


    constructor(props) {
        super(props)

    }


    render() {


        const {email, password} = this.props.auth
        return (
            <View style={{
                flex: 1,
                backgroundColor: purpleTitle,
                alignItems: 'center',
                justifyContent: 'center',
                padding: 20
            }}>

                <Image
                    resizeMode={'contain'}
                    source={require('../../assets/images/horizontallogo.png')} style={{width: '100%', height: 150}}/>


                <Text style={{fontSize: 18, color: 'white'}}>A Equipe Vendedor Solar Agradece seu cadastro, para
                    concluir, um link de confirmação foi enviado
                    para o email</Text>
                <Text style={{fontSize: 20, margin: 10}}>{email}</Text>
                <Text style={{fontSize: 18, color: 'white'}}>Caso não tenha recebido, verifique na caixa de span ou
                    solicite reenvio através do botão.</Text>


                <View style={{marginTop: 20}}>
                    <Button
                        color={logoBlue}
                        onPress={() => {
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate(
                                    {
                                        routeName: 'main',
                                    })],
                            });
                            this.props.navigation.dispatch(resetAction,);
                        }}
                        label={'Continuar'}/>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({});


const mapStateToProps = (state) => {

    return {
        auth: state.auth.auth,

    }
}


export default connect(mapStateToProps, {})(WelcomeScreen)



