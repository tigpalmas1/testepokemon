import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    Dimensions,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';


import {Header} from "../../components/Header";

import CartItem from "../cart/CartIem";
import {TagButton} from "../../components/common/TagButton";
import {Button} from "../../components/common/Button";
import {emptyContainer} from "../../components/common/styles/geralStyle";
import EmptyState from "../fragment/EmptyState";
import {purpleTitle} from "../../components/common/colors/index";
import AddressSelected from './AddressSelected';


export default CartScreen = (props) => {
    const cart = useSelector(state => state.cart.cart);
    const {loading, message, navigation} = props || {}


    renderItem = (item, index) => {
        console.log('entrou aqui')
        return (


            <CartItem
                product={item}
                onPressEdit={() => navigation.navigate('editCard', {product: 'true'})}
                onPressCard={() => {
                    this.props.setKit(item)
                    this.props.navigation.navigate('kitDetail')
                }}/>

        )
    }


    return (


        <View style={styles.container}>
            <Header
                backButton
                onPress={() => {
                    navigation.goBack(null)
                }}

                title={'Seu Carrinho '}


            />


            {cart.length === 0 &&
            <View style={emptyContainer}>
                <EmptyState
                    message={'Você ainda não adicionou nenhum Produto na sacola'}
                />
            </View>
            }


            <View style={{flex: 1, paddingHorizontal: 20}}>
                <View style={{
                    width: '100%', flexDirection: 'row', justifyContent: 'space-between',
                    alignItems: 'center'
                }}>

                    <Text style={{fontWeight: '700', fontSize: 18}}>Total</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <TagButton
                            color={purpleTitle}
                            title={'R$ 96,96'}
                        />

                        <Button

                            onPress={() => {
                            }}
                            textColor={purpleTitle}
                            label="Confirmar"/>


                    </View>

                </View>

                <View style={{
                    width: '100%', flexDirection: 'row',

                }}>
                    <AddressSelected/>


                </View>


                <FlatList

                    refreshing={false}
                    onRefresh={() => console.log('refreshing')}
                    keyExtractor={(item, index) => item._id}
                    showsVerticalScrollIndicator={false}
                    style={{flex: 1,}}
                    data={cart}
                    onEndReached={this.handleLoadMore}
                    onEndThreshold={0}
                    renderItem={({item, index}) => this.renderItem(item, index)}


                />
            </View>

        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",


    },
    header: {
        marginTop: 20,
        height: 200,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },

});




