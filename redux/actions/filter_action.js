import {
    ESTABLISHMENT_COORDENATES_UPDATE, ESTABLISHMENT_IMAGE_UPDATE,
    ESTABLISHMENT_LOADING, ESTABLISHMENT_SAVE_ERROR, ESTABLISHMENT_SAVE_SUCCESS,
    ESTABLISHMENT_UPDATE, FETCH_ESTABLISHMENT, FILTER_UPDATE, FILTER_UPDATE_TYPE, INSTALER_UPDATE, LOGIN_SUCCESS,
    SET_FILTER, STATE_CHANGE_ACTION,

} from "./types";
import {getStorage, setStorage} from "../../components/common/functions/index";
import axios from 'axios'


export const checkFilter = (callbackFilter) => async (dispatch) => {

    const filter = await getStorage("filter");


    if (!filter) {
        callbackFilter(false)
    }else {

       await dispatch({
            type: SET_FILTER,
            payload:  filter

        })
        callbackFilter(filter)
    }
};


export const filterUpdateAction = ({prop, value}) => async (dispatch) => {
    dispatch({
        type: FILTER_UPDATE,
        payload: {prop, value}
    })
};

export const filterTypeAction = (type) => async (dispatch) => {
    dispatch({
        type: FILTER_UPDATE_TYPE,
        payload: type
    })
};



export const setFilterAction = (filter) => async (dispatch) => {
    try {
        await  setStorage("filter", filter)
        dispatch({
            type: SET_FILTER,
            payload:  filter

        })

    } catch (e) {
        checkResponseError(e)
    }
};


export const stateChangedAction = (status) => async (dispatch) => {
    dispatch({
        type: STATE_CHANGE_ACTION,
        payload:  status
    })
};






