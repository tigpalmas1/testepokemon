import {

    FETCH_KITS_LOADING, FETCH_KITS, LOGOUT, CLEAR_KITS_LIST, SET_KIT, ADD_ITEM, REMOVE_ITEM,
    FETCH_KITS_PROMOIONAL_LOADING, FETCH_KITS_PROMOTIONAL,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    loading: false,
    cart: [],
    message: '',


}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {

        case 'ADD_CART_ITEM':
            return {
                ...state,
                cart:[
                    ...state.cart,
                    payload

                ]
            }
        case REMOVE_ITEM:
            return {
                ...state,
                kit:{
                    ...state.kit,
                    itemId: state.kit.itemId.map(
                        (item, i) => item._id === payload ? {...item, quantity: item.quantity-1}: item
                    )
                }
            }



        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

