import React, {useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
    TouchableHighlight,
    Keyboard,
    ActivityIndicator,
    TouchableOpacity,
    Alert
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


import {
    instalerUpdateAction, searchCep, installerSaveAction, _getLocationAsync,addAddressAction
} from "../../redux/actions";
import {connect} from "react-redux";


import {Header} from "../../components/Header";
import {
    buttonWrapperEditProfile,
    errorMessageContainer, errorMessageStyle, imageContainer, imagePickerContainer,
    nameLastNameContainer
} from "../../components/common/styles/perfilStyle";
import {grey, logoBlue, purpleTitle, titleGrey} from "../../components/common/colors/index";
import {Input} from "../../components/common/Input";
import {InputMask} from "../../components/common/InputMask";
import {Button} from "../../components/common/Button";
import ConfirmationDialog from '../dialogs/InstalerThermsDialog'
import {Card,} from 'react-native-paper';
import {StackActions, NavigationActions} from 'react-navigation';
import LoadingDialog from "../dialogs/LoadingDialog";


const AddressScreen = (props) => {
    const {loadingCoordinates, setloadingCoordinates} = useState(false);
    const {loading, setloading} = useState(false);
    const [searchingCep, setsearchingCep] = useState(false);
    const {cnpjError, setcnpjError} = useState('');
    const {streetError, setstreetError} = useState('');
    const {neighError, setneighError} = useState('');
    const {cityError, setcityError} = useState('');
    const {stateError, setstateError} = useState('');
    const {numberError, setnumberError} = useState('');
    const {nameError, setnameError} = useState('');
    const {cepError, setcepError} = useState('');
    const {dialogVisible, setdialogVisible} = useState(false);
    const {locationSearch, setlocationSearch} = useState(false);

    const {installerSaveAction, user} = props


    const { errorMessage, navigation, instalerUpdateAction, addAddressAction} = props || {}
    const {_id, neighborhood, cep, city, state, number, street} = user || {}
    const {container, content, title, section, pickerWrapper, pickkerText, picker, inputWrapper, labelStyle} = styles;


    const callbackLocation = (object) => {
        setlocationSearch(false)
        const {message, userLocation} = object
        if (message) {
            alert(message)
        } else if (userLocation) {
            const {coords} = userLocation || {}
            const {latitude, longitude} = coords || {}
            if (latitude && longitude) {

            }
        }
    }


    const onPressButton = () => {
        addAddressAction(user)

       /* if (!this.checkError()) {
            setloading(true)
            installerSaveAction(_id, user, callback);
        }*/
    };

    const callback = (message) => {
        setloading(false)

    }


    const checkError = () => {
        const {user,} = this.props || {}
        const {neighborhood, cep, city, state, number, street} = user || {}

        let errors = false;


        if (!neighborhood) {
            this.setState({neighError: 'Campo obrigátorio'});
            errors = true
        }

        if (!cep) {
            this.setState({cepError: 'Campo obrigátorio'});
            errors = true
        }
        if (!street) {
            this.setState({streetError: 'Campo obrigátorio'});
            errors = true
        }
        if (!city) {
            this.setState({cityError: 'Campo obrigátorio'});
            errors = true
        }
        if (!state) {
            this.setState({stateError: 'Campo obrigátorio'});
            errors = true
        }
        if (!number) {
            this.setState({numberError: 'Campo obrigátorio'});
            errors = true
        }

        return errors
    }


    callbackAddress = (message) => {
        setsearchingCep(false)

    }


    const searchCep = () => {

        if (!cep) {
            setcepError('Campo obrigátorio');
            errors = true
        } else {
            setsearchingCep(true);
            props.searchCep(this.cepRef.getRawValue(), callbackAddress)
        }
    }





        return (
            <View style={container}>
                <View>

                    <Header

                        title={"Endereço de Entrega"}
                        backButton
                        onPress={() => {
                            navigation.goBack(null)
                        }}
                    />
                </View>


                <KeyboardAwareScrollView>
                    <View>
                        <View style={content}>
                            <Card style={{
                                elevation: 11,
                                width: '100%',
                                marginTop: 20,
                                paddingHorizontal: 10,
                                paddingVertical: 10
                            }}>
                                <View style={{flex: 1}}>


                                    {/*CELULAR */}


                                    <View style={{width: '100%'}}>


                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                width: '100%',
                                                flex: 1,
                                                alignItems: 'space-between',
                                            }}>


                                            <InputMask
                                                setRef={(input) => this.cepRef = input}
                                                onSubmitEditing={() => {
                                                    this.streetRef.focus();
                                                }}
                                                value={cep}
                                                placeHolder='XXXXX-XXX'
                                                error={cepError}
                                                type={'zip-code'}
                                                label="CEP"
                                                onChangeText={value => {

                                                    instalerUpdateAction({prop: 'cep', value})
                                                }}

                                            />


                                            <TouchableOpacity
                                                disabled={searchingCep}
                                                onPress={() => searchCep()}
                                                style={{width: 40, height: 40}}>

                                                {!searchingCep &&
                                                <Icon

                                                    size={24}
                                                    name={"magnify"}
                                                    style={{color: 'grey', flexWrap: 'wrap', marginLeft: 10}}
                                                />
                                                }

                                                {searchingCep &&
                                                <ActivityIndicator
                                                    color={'grey'}
                                                    size='small'
                                                />
                                                }

                                            </TouchableOpacity>


                                        </View>
                                        <View style={nameLastNameContainer}>
                                            <View style={{flex: 7}}>
                                                <Input
                                                    setRef={(input) => this.streetRef = input}
                                                    onSubmitEditing={() => {
                                                        this.numberRef.focus();
                                                    }}
                                                    label={'Rua'}
                                                    value={street}
                                                    error={streetError}
                                                    onChangeText={value => {

                                                        instalerUpdateAction({prop: 'street', value})
                                                    }}

                                                />
                                            </View>

                                        </View>

                                        <View style={nameLastNameContainer}>
                                            <View style={{flex: 3}}>
                                                <Input
                                                    setRef={(input) => this.numberRef = input}
                                                    onSubmitEditing={() => {
                                                        this.neighboorRef.focus();
                                                    }}
                                                    label={'Número'}
                                                    value={number ? number.toString() : ''}
                                                    error={numberError}
                                                    onChangeText={value => {

                                                        instalerUpdateAction({prop: 'number', value})
                                                    }}

                                                /></View>
                                            <View style={{flex: 6}}>
                                                <Input
                                                    setRef={(input) => this.neighboorRef = input}
                                                    onSubmitEditing={() => {
                                                        this.cityRef.focus();
                                                    }}
                                                    label={'Bairro'}
                                                    value={neighborhood}
                                                    error={neighError}
                                                    onChangeText={value => {

                                                        instalerUpdateAction({prop: 'neighborhood', value})
                                                    }}

                                                />
                                            </View>

                                        </View>


                                        <View style={nameLastNameContainer}>

                                            <View style={{flex: 4}}>
                                                <Input
                                                    setRef={(input) => this.cityRef = input}
                                                    onSubmitEditing={() => {
                                                        this.stateRef.focus();
                                                    }}
                                                    label={'Cidade'}
                                                    value={city}
                                                    error={cityError}
                                                    onChangeText={value => {

                                                        instalerUpdateAction({prop: 'city', value})
                                                    }}

                                                /></View>

                                            <View style={{flex: 2}}>
                                                <Input
                                                    setRef={(input) => this.stateRef = input}
                                                    onSubmitEditing={() => {
                                                        Keyboard.dismiss()
                                                    }}
                                                    label={'Estado'}
                                                    value={state}
                                                    error={stateError}
                                                    onChangeText={value => {

                                                        instalerUpdateAction({prop: 'state', value})
                                                    }}

                                                /></View>
                                        </View>
                                    </View>

                                </View>
                            </Card>


                            <View style={errorMessageContainer}>
                                <Text style={errorMessageStyle}>{errorMessage}</Text>
                            </View>


                            {/*BUTTON SAVE*/}
                            <View style={buttonWrapperEditProfile}>

                                <Button
                                    color={logoBlue}
                                    disabled={loading}
                                    onPress={onPressButton}
                                    label={loading ? 'Salvando aguarde..' : 'Salvar Alterações'}/>
                            </View>

                        </View>
                    </View>


                </KeyboardAwareScrollView>


            </View>


        );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        padding: 10,
    },
    title: {
        fontWeight: '700', fontSize: 16,
    },
    inputWrapper: {
        margin: 3,
        width: '100%',

        borderBottomWidth: 0.5,
        borderColor: purpleTitle,
        padding: 3,

    },
    labelStyle: {
        color: titleGrey,
        marginLeft: 5,
        fontSize: 16,
        fontWeight: '700',
    },
    section: {marginTop: 20},
    pickerWrapper: {width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 10},
    pickkerText: {fontSize: 14, fontWeight: '500', flex: 8, color: titleGrey,},
    picker: {flex: 2}

});

const mapStateToProps = (state) => {

    return {
        user: state.user.user,
        errorMessage: state.instaler.errorMessage,
        loading: state.instaler.loading
    }
};


export default connect(mapStateToProps, {
    addAddressAction,
    instalerUpdateAction,
    searchCep,
    installerSaveAction,
    _getLocationAsync
})(AddressScreen)


