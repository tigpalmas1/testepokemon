import {
    ADD_USER, ADD_USER_LOADING, AUTH_UPDATE, CLEAR_USER, FETCH_USERS, FETCH_USERS_LOADING, LOGIN_SUCCESS, LOGOUT,
    SET_UPDATE_USER, SIGNUP_SUCCESS, UPDATER_USER_LOCATION, IMAGE_USER_UPDATE, BANNER_IMAGE_UPDATE, USER_INFO_UPDATE,
    INSTALER_UPDATE
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    user:{},
    permission:{}


}

export default function (state = INITIAL_STATE, {type,payload}) {
    switch (type) {
          case INSTALER_UPDATE:

            return {
                ...state,
                user: {
                    ...state.user,
                    [payload.prop]: payload.value
                },
            }


        case SIGNUP_SUCCESS:
            return {
                ...state,
                data: payload
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                user: payload.user,
                permission: payload.permission,
            }
       case IMAGE_USER_UPDATE:
            console.log(payload)

            return {
                ...state,
                data: {
                    ...state.data,
                    user: {
                        ...state.data.user,
                        imageUser: payload
                    }

                }
            }

        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

