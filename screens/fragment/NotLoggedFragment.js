import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, Keyboard} from 'react-native';
import {connect} from 'react-redux'
import {Card,} from 'react-native-paper';
import {InputLogin} from "../../components/common/InputLogin";
import {Button} from "../../components/common/Button";
import {logoBlue} from "../../components/common/colors/index";
import {seeMoreContainer} from "../../components/common/styles/geralStyle";
import {authUpdate,signupServer, loginServerAction} from "../../redux/actions/auth_action";


const {width} = Dimensions.get('window');

const NotLoggedFragment = (props) => {

    const [loginState, setLoginState] = useState(true)

    const {authUpdate, signupServer, loginServerAction} = props ||{}
    const {auth} = props ||{}

    const {name,  email,  password, confirmPassword,} = auth || {};


    const onPresssButton = () => {
        if(loginState){
            loginServerAction(auth)
        }else{
            signupServer(auth)
        }
    }

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>


            <Card style={{width: "90%", height: loginState? 300: 400, backgroundColor: 'pink',}}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>

                    {!loginState &&
                    <View style={{flexDirection: 'row', width: '100%', marginTop: 5,}}>
                        <InputLogin

                            icon={"account"}
                            setRef={(input) => this.confirmPassRef = input}
                            onSubmitEditing={() => {
                                Keyboard.dismiss()
                            }}
                            error={''}
                            label={'Nome Completo'}
                            placeholder={'Confirme seu nome completo'}
                            value={name}

                            onChangeText={value => {
                                authUpdate({prop: 'name', value})
                            }}

                        />
                    </View>
                    }

                    <View style={{flexDirection: 'row', width: '100%', marginTop: 5,}}>
                        <InputLogin

                            icon={"email"}
                            setRef={(input) => this.emailRef = input}
                            onSubmitEditing={() => {
                                this.passRef.focus();
                            }}
                            error={''}
                            label={'Email'}
                            placeholder={'Digite seu email'}
                            value={email}

                            onChangeText={value => {
                                authUpdate({prop: 'email', value})

                            }}

                        />
                    </View>


                    <View style={{flexDirection: 'row', width: '100%', marginTop: 5,}}>
                        <InputLogin

                            icon={"lock"}
                            setRef={(input) => this.passRef = input}
                            onSubmitEditing={() => {
                                this.confirmPassRef.focus();
                            }}
                            error={''}
                            label={'Senha'}
                            placeholder={'Digite sua senha'}
                            value={password}

                            onChangeText={value => {
                                authUpdate({prop: 'password', value})
                            }}

                        />
                    </View>

                    {!loginState &&
                    <View style={{flexDirection: 'row', width: '100%', marginTop: 5,}}>
                        <InputLogin

                            icon={"lock"}
                            setRef={(input) => this.confirmPassRef = input}
                            onSubmitEditing={() => {
                              Keyboard.dismiss()
                            }}
                            error={''}
                            label={'Repita a senha'}
                            placeholder={'Confirme a sua senha'}
                            value={confirmPassword}

                            onChangeText={value => {
                                authUpdate({prop: 'confirmPassword', value})
                            }}

                        />
                    </View>
                    }


                    <View style={{marginVertical: 10,}}>


                        <Button
                            color={logoBlue}
                            disabled={false}
                            onPress={onPresssButton}
                            label={loginState ? 'Entrar': 'Criar Conta'}/>
                    </View>

                    <View style={{width: '100%', alignItems: 'center', marginTop: 10}}>
                        <TouchableOpacity style={seeMoreContainer}
                                          onPress={() =>setLoginState(!loginState)}
                        >
                            <Text style={{
                                paddingHorizontal: 20,
                                textAlign: "center",
                                marginBottom: 10,
                                fontWeight: '700',
                                fontSize: 14,
                                color: 'white'
                            }}>{loginState? 'Ainda não é cadastrado? clique aqui para criar uma conta.' : 'Já tenho uma conta'}</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </Card>


        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        flexWrap: 'wrap',
        padding: 10
    }

});


const mapStateToProps = (state) => {
    console.log(state.user.permission)
    return {
        auth: state.auth.auth,
        user: state.user.user,
        permission: state.user.permission,
        logged: state.auth.logged,
        loadingSignup: state.auth.loadingSignup,
        errorSignup: state.auth.errorSignup,
    }
}


export default connect(mapStateToProps, {authUpdate, loginServerAction, signupServer

})(NotLoggedFragment)


