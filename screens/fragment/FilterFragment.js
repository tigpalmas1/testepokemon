import React from 'react';
import {StyleSheet, View, ScrollView,Text} from 'react-native';
import {connect} from "react-redux";
import {getFormatedDate, revertFilterData} from "../../components/common/functions/index";
import {GradientTagButton} from "../../components/common/GradientTagButton";


class FilterFragment extends React.Component {

    constructor(props) {
        super(props);
        state = {
            location: null,
            errorMessage: null,
        };
    }





    checkLabel = (orderBy)=>{
       if (orderBy === 'priority') return 'Principais'
       if (orderBy === 'valueUp') return 'Por Valor'
       if (orderBy === 'promotion') return 'Em promoção'
    }

    getPlaceholder = () =>{
        const {searchType} = this.props.filter || {}
        if(searchType ==='kWh'){
            return 'kWh'
        }else  if (searchType === 'modulesNumber') {
            return 'Módulos'
        }else  if (searchType === 'kWp') {
            return 'kWp'
        }
    }


    render() {
        const {filter, onFilterPress } = this.props || {};
        const {searchType, value, state, city, fixedType, orderBy} = filter || {}






        const {container,} = styles;
        return (
            <View style={container}>
                <ScrollView
                    contentContainerStyle={{alignItems: 'center',}}
                    ref='_scrollView'
                    horizontal
                    showsHorizontalScrollIndicator={false}
                >


                    <GradientTagButton
                        onPress={onFilterPress}
                        title={this.checkLabel(orderBy)}
                        iconName={'filter'}
                    />
                    <Text style={{fontWeight:'700', color: 'grey',fontStyle: 'italic', marginLeft: 5 }}>{value}</Text>
                    <Text style={{fontWeight:'700', color: 'grey',fontStyle: 'italic',  }}>{this.getPlaceholder(searchType)} -</Text>
                    <Text style={{fontWeight:'700', color: 'grey',fontStyle: 'italic', marginLeft: 5 }}>{fixedType}</Text>

                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: 5,
        marginTop: -10,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },
});

const mapStateToProps = (state) => {

    return {
        filter: state.filter.filter
    }
};


export default connect(mapStateToProps, null)(FilterFragment)