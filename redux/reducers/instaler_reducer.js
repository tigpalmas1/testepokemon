import {
    AUTH_UPDATE, LOGIN_LOADING, LOGIN_ERROR,
    LOGOUT, LOGIN_SUCCESS, SIGNUP_LOADING, SIGNUP_SUCCESS, SIGNUP_ERROR, AUTH_LOADING, INSTALER_UPDATE,
    BANNER_IMAGE_UPDATE, LOGO_IMAGE_UPDATE, SET_SELECTED_INSTALER, FETCH_INSTALER_LOADING, FETCH_INSTALER,
    CARD_IMAGE_UPDATE, CLEAR_INSTALER_LIST, SET_INSTALER,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    instaler: {
        corporateName: '',
        cnpj: '',
        phone: '',
        street: '',
        number: '',
        cep: '',
        neighborhood: '',
        state: '',
        city: '',
        cardImage: {
            url: ''
        },

    },

    instalers: [],
    message: '',
    loading: false,

}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {
      /*  case INSTALER_UPDATE:
            return {
                ...state,
                instaler: {
                    ...state.instaler,
                    [payload.prop]: payload.value
                },
            }*/

        case SET_INSTALER:
            return {
                ...state,
                instaler: payload
                

            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                instaler: payload.userJson,
            }

        case CARD_IMAGE_UPDATE:

            return {
                ...state,
                instaler: {
                    ...state.instaler,
                    cardImage: payload
                }

            }

        case CLEAR_INSTALER_LIST:

            return {
                ...state,
                instalers:[]

            }


        case FETCH_INSTALER_LOADING:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading
            }

        case FETCH_INSTALER:
            return {
                ...state,
                instalers: _.uniqBy(state.instalers.concat(payload.instalers), "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

