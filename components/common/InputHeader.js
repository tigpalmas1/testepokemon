import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'



const InputHeader = ({
                         label, error, value, onChangeText,
                         placeholder,
                         secureTextEntry,
                         onPressClose,
                         icon,
                         autoCapitalize, setRef, onSubmitEditing
                     }) => {

    const {inputStyle, labelStyle, containerStyle, inputWrapper,} = styles;

    return (
        <View style={containerStyle}>



                <View style={[inputWrapper, {borderColor: titleGrey}]}>

                    <Icon
                        size={24}
                        name={"magnify"}
                        style={{color: 'grey', flexWrap: 'wrap', marginLeft: 10}}
                    />

                    <TextInput
                        secureTextEntry={secureTextEntry}
                        autoCapitalize={autoCapitalize}
                        autoCorrect={false}
                        underlineColorAndroid='transparent'
                        placeholder={"Buscar Tickets"}
                        placeholderTextColor={titleGrey}
                        value={value}
                        onChangeText={onChangeText}
                        onSubmitEditing={onSubmitEditing}
                        ref={setRef}
                        blurOnSubmit={false}
                        style={inputStyle}/>

              {/*      <TouchableOpacity onPress={onPressClose}>
                        <Icon
                            size={24}
                            name={"close"}
                            style={{color: 'grey', flexWrap: 'wrap', marginRight: 10}}
                        />
                    </TouchableOpacity>*/}


                </View>




        </View>
    )
}

const styles = {
    containerStyle: {
        flex: 1,

    },
    inputWrapper: {

        width: '100%',
        height: '100%',

        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 10,





        borderWidth: 1,


    },
    inputStyle: {
        padding: 10,


        flex: 1,
        color: titleColor,
        fontSize: 14,
        fontWeight: '700'
    },


}

export {InputHeader};