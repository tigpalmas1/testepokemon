import React, {Component} from 'react';
import {
    View, Text, Modal, TouchableOpacity, ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {connect} from 'react-redux'


import {Button} from "../../components/common/Button";

import {grey, purpleTitle, titleColor} from "../../components/common/colors/index";
import {dialogText} from "../../components/common/styles/geralStyle";

import {
    blackBackground, buttonsContainer, buttonsWrapper,
    cancelButtonStyle, iconStyle, titleWrapper, whiteContainer,
    whiteTitleStyle
} from "../../components/common/styles/dialogStyles";
import {Input} from "../../components/common/Input";
import {categoryUpdate, addCategoryAction} from "../../redux/actions";
import {intalerTherms} from "../../components/common/constants/index";
import {CheckBox} from "../../components/common/CheckBox";


const LoadingDialog = ({title, iconName, text, onPressCancel, onPressConfirm, visible, cancelButton, textConfirm}) => {


    return (
        <Modal
            onRequestClose={() => {
            }}
            animationType='slide' transparent visible={visible}>
            <View style={blackBackground}>
                <View style={whiteContainer}>


                    <View style={titleWrapper}>
                        <Icon
                            size={20}
                            name={iconName}
                            style={iconStyle}
                        />
                        <Text style={whiteTitleStyle}> {title} </Text>

                    </View>


                    <View>
                        <View style={{padding: 20, alignItems:'center', justifyContent:'center'}}>
                            <ActivityIndicator/>



                        </View>


                    </View>


                </View>
            </View>
        </Modal>
    )


}

const styles = {
    loading: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'

    },
    tag: {
        fontSize: 10,
        color: 'white',
        fontWeight: '700'


    },

    tagWrapper: {
        padding: 5,

        backgroundColor: purpleTitle,
        flexWrap: 'wrap',
        borderRadius: 12,
        marginRight: 5
    },

    tagGreyWrapper: {
        padding: 5,
        backgroundColor: grey,
        flexWrap: 'wrap',
        borderRadius: 12,
        marginRight: 5
    }


}

export default LoadingDialog;





