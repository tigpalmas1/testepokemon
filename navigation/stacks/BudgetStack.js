import React from 'react';

import {createStackNavigator} from 'react-navigation';
import TabBarIcon from "../../components/TabBarIcon";
import BudgetListScreen from '../../screens/order/OrderListScreen'
import OrderDetailScreen from '../../screens/order/OrderDetailScreen'


const BudgetStack = createStackNavigator({
    budget: BudgetListScreen,
    orderDetail: OrderDetailScreen


});

BudgetStack.navigationOptions =({navigation}) =>{

    let tabBarVisible = true;
    if(navigation.state.index >0){
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
        tabBarLabel: 'Pedidos',
        tabBarIcon: ({ focused }) => (

            <TabBarIcon
                focused={focused}
                name={'message'}
            />
        ),
    };
};

export default BudgetStack