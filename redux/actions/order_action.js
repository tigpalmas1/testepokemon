import {
    ADD_ITEM,
    CLEAR_KITS_LIST,
    ESTABLISHMENT_COORDENATES_UPDATE, FETCH_KITS, FETCH_KITS_LOADING, FETCH_ORDERS, FETCH_ORDERS_LOADING, REMOVE_ITEM,
    SET_KIT,

} from "./types";
import {checkResponseError, getStorage, setStorage} from "../../components/common/functions/index";
import axios from 'axios'
import {Location, Permissions, IntentLauncherAndroid} from 'expo';
import {baseUrl} from "../../components/common/constants/index";
import _ from "lodash"

export const fetchOrdersAction = ( skip, isSeller) => async (dispatch) => {
    if(isSeller){
        dispatch({type: FETCH_ORDERS_LOADING, payload: {loading: false, message: 'Vendedores ainda não podem fazer pedidos :('}})
    }else{
        dispatch({type: FETCH_ORDERS_LOADING, payload: {loading: true, message: 'Buscando Solicitações de Kits'}})


        try {
            var url = `${baseUrl}/cart?`;
            let config = '';


            config = {
                params: {
                    skip: skip,
                    limit: 10
                },
            }
            const {data} = await axios.get(url, config)

            console.log(data)

            dispatch({
                type: FETCH_ORDERS,
                payload: {
                    orders: data,
                    message: data.length > 0 ? '' : 'Você ainda não solicitou nenhum Kit',
                    endList: data.length< 10
                }
            });

        } catch (e) {

            dispatch({type: FETCH_ORDERS_LOADING, payload: {loading: false, message: checkResponseError(e)}})

        }
    }


};

export const saveOrderAction = (_id, kit, callback, selectedInstaler) => async (dispatch) => {

    selectedInstaler? kit.installerId = selectedInstaler._id : null

    try {
        var url = `${baseUrl}/cart`;
        await axios.post(url, kit)


        const {data} = await axios.get(url)

        console.log(data)

        dispatch({
            type: FETCH_ORDERS,
            payload: {
                orders: data,

            }
        });
        callback('Kit Solicitado com sucesso')

    } catch (e) {

        callback(checkResponseError(e))

    }
};


export const cancelOrderAction = (orderId, callback, _id) => async (dispatch) => {


    try {
        var url = `${baseUrl}/cart`;
        await axios.put(url+`/${orderId}`,{status: 'Cancelado'})

        const {data} = await axios.get(url,)

        console.log(data)

        dispatch({
            type: FETCH_ORDERS,
            payload: {
                orders: data,
            }
        });
        callback('Ordem Cancelada com Sucesso')

    } catch (e) {

        callback(checkResponseError(e))

    }
};

