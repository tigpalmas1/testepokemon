import React  from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { Card,} from 'react-native-paper';
import {ImagePicker, LinearGradient, Permissions} from 'expo'
import {
    formataDinheiro
    , getNationalValue,
    } from "../../components/common/functions/index";


const KitCardItem = (props) => {
        const {
            onPressEdit
    } = props || {};


    return (

        <TouchableWithoutFeedback onPress={()=>{}}>
            <Card style={{elevation: 11, width: '100%', marginRight: 10,  }}>

                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>


                    <Image
                        reziseMode={'contain'}
                        style={{width: 80, height: 80}}
                        source={{uri: 'https://elcopcbonline.com/photos/product/4/176/4.jpg'}}/>
                    <View style={{paddingVertical: 10, flex: 2}}>

                            <View>
                                <Text style={{
                                    fontWeight: '700',
                                    fontSize: 14,
                                    color: titleColor
                                }}> {'Fone Alto padrão'}</Text>
                                <Text
                                    style={{fontWeight: '500', fontSize: 14, color: titleGrey}}> R$50,00 </Text>
                            </View>

                    </View>

                    <View style={{paddingVertical: 10, flexDirection:'row', paddingHorizontal: 20}}>

                        <TouchableOpacity
                            style={{marginRight: 15}}
                            onPress={onPressEdit}
                        >
                            <Icon
                                size={24}
                                name={'pencil-outline'}
                            />

                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={()=>{}}
                        >
                            <Icon
                                size={24}
                                name={'delete'}
                            />

                        </TouchableOpacity>

                    </View>
                </View>



            </Card>
        </TouchableWithoutFeedback>

    )


}

const styles = StyleSheet.create({});


export default KitCardItem
