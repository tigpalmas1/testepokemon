import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Platform, StatusBar, ActivityIndicator, Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {purpleTitle, titleColor} from "./common/colors/index";
import {InputHeader} from "./common/InputHeader";
import {useDispatch, useSelector} from 'react-redux';
import {EBTExt} from "./EBText";


const Header = (props)=> {
    const cart = useSelector(state => state.cart.cart);


    const {
        backButton, colorText, title, onPress, iconRigthName, onIconPress, cartIcon,onCartPress
    } = props



    const renderBackButton = (backButton, onPress) => {

        if (backButton) {
            return (
                <TouchableOpacity onPress={onPress}>
                    <Icon
                        style={{marginRight: 10}}
                        color={'black'}
                        size={24}
                        name="arrow-left"
                    />
                </TouchableOpacity>
            )
            return (
                null
            )
        }

    }



        const {header, inputWrapper} = styles;



        return (


            <View style={[header]}>

                <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                    {renderBackButton(backButton, onPress)}

                   {/* {logo &&
                    <Image
                        resizeMode={'contain'}
                        source={require('../assets/images/horizontallogo.png')} style={{width: 120, height: 55}}/>
                    }*/}


                    <EBTExt style={ {
                        color: colorText,
                        fontSize: 20,
                        fontWeight: '700',
                    }}>{title}</EBTExt>


                </View>


                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>

                    {iconRigthName &&
                    <TouchableOpacity
                        onPress={onIconPress}
                    >
                        <Icon
                            size={24}
                            name={iconRigthName}
                        />

                    </TouchableOpacity>

                    }

                    {cartIcon &&
                    <TouchableOpacity
                        onPress={onCartPress}
                    >
                        <Icon
                            size={24}
                            name={'settings'}
                        />

                        {cart.length > 0 &&
                        <View style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            backgroundColor: purpleTitle,
                            padding: 2,
                            borderRadius: 10
                        }}>
                            <Text style={{color: 'white', fontWeight: '700', fontSize: 8}}>{cart.length}</Text>
                        </View>
                        }
                    </TouchableOpacity>

                    }


                </View>

            </View>

        )




}

const styles = {
    header: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20,
        height: 55,


    },
    title: {
        color: 'white',
        fontSize: 20,
        fontWeight: '700',
    }


}

export {Header};