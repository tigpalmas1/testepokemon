import React from 'react';
import { Text } from 'react-native';

export function RBText(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'nunito-regular' }]} />
  );
}
