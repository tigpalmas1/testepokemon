import React from 'react';
import { Text } from 'react-native';

export function EBTExt(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'font-extra-bold' }]} />
  );
}
