import React,{useState}  from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { Card,} from 'react-native-paper';
import {ImagePicker, LinearGradient, Permissions} from 'expo'
import {
    formataDinheiro
    , getNationalValue,
    } from "../../components/common/functions/index";
import {TagButton} from "../../components/common/TagButton";


const CategoryItem = (props) => {

        const {removeCategoryPress, onPressAddProduct} = props || {};
        const {item} = props || {};
        const {name: nameCategory} = item || {};

        const [ìsExpanded, setìsExpanded] = useState(false);



    return (

        <TouchableWithoutFeedback onPress={()=>setìsExpanded(!ìsExpanded)}>
            <Card style={{elevation: 11, width: '100%',   }}>

                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{padding: 10, flex: 2}}>
                            <View>
                                <Text style={{
                                    fontWeight: '700',
                                    fontSize: 14,
                                    color: titleColor
                                }}> {nameCategory}</Text>
                            </View>
                    </View>

                    <View style={{paddingVertical: 10, flexDirection:'row', paddingHorizontal: 20}}>
                        <TouchableOpacity
                            onPress={removeCategoryPress}
                        >
                            <Icon
                                size={20}
                                name={'delete'}
                            />

                        </TouchableOpacity>

                    </View>
                </View>

                {ìsExpanded &&
                <View style={{flex: 1, width: '100%',flexDirection: 'row',padding: 10, justifyContent: 'flex-end'}}>
                    <TagButton
                        onPress={()=>{onPressAddProduct(item)}}
                        title={'Add Produto'}
                        color={purpleTitle} />
                </View>

                }



            </Card>
        </TouchableWithoutFeedback>

    )


}

const styles = StyleSheet.create({});


export default CategoryItem
