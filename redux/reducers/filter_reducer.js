import {
    AUTH_UPDATE, LOGIN_LOADING, LOGIN_ERROR,
    LOGOUT, LOGIN_SUCCESS, SIGNUP_LOADING, SIGNUP_SUCCESS, SIGNUP_ERROR, AUTH_LOADING, INSTALER_UPDATE, FILTER_UPDATE,
    FILTER_UPDATE_TYPE, SET_FILTER, STATE_CHANGE_ACTION,

} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    filter: {
        searchType: 'kWp',
        value : '1,36',
        state: 'Paraná',
        city: 'Curitiba',
        fixedType: 'Telha Cerâmica',
        orderBy: 'priority',
    },
    stateChanged: true,
}

export default function (state = INITIAL_STATE, {type, payload}) {
    switch (type) {
        case FILTER_UPDATE:
            return {
                ...state,
                filter: {
                    ...state.filter,
                    [payload.prop]: payload.value
                },
            }
        case FILTER_UPDATE_TYPE:{
            return {
                ...state,
                filter: {
                    ...state.filter,
                   orderBy: payload
                },
            }
        }

        case SET_FILTER:{
            return {
                ...state,
                filter: payload
            }
        }
        case STATE_CHANGE_ACTION:
            return {...state,
                stateChangend: payload
            }

        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

