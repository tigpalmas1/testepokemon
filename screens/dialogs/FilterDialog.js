import React, {Component} from 'react';
import {
    View, Text, Modal, TouchableOpacity, StyleSheet, Keyboard, TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {connect} from 'react-redux'


import {Button} from "../../components/common/Button";

import {grey, purpleTitle, titleColor, titleGrey} from "../../components/common/colors/index";
import {dialogText} from "../../components/common/styles/geralStyle";

import {
    blackBackground, buttonsContainer, buttonsWrapper,
    cancelButtonStyle, iconStyle, titleWrapper, whiteContainer,
    whiteTitleStyle
} from "../../components/common/styles/dialogStyles";
import {Input} from "../../components/common/Input";
import {filterTypeAction,stateChangedAction, setFilterAction} from "../../redux/actions";

import {CalcPicker} from "../../components/common/CalcPicker";
import {InputLogin} from "../../components/common/InputLogin";
import myJson from '../../components/common/constants/estados-cidades.json'
import RNPickerSelect, {defaultStyles} from 'react-native-picker-select';
import {fixedType, fixedTypeItems} from "../../components/common/constants/index";
import _ from 'lodash';
import {GradientTagButton} from "../../components/common/GradientTagButton";
import {TagButton} from "../../components/common/TagButton";


class FilterDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saving: false,
            consumeError: '',
            checked: false,
            cities: [],
            states: []

        }
        //console.log(myJson.estados)


    }


    pressFilter = (type) => {
        const {stateChangedAction, filterTypeAction, dismissDialog, setFilterAction, filter} = this.props


        filterTypeAction(type)

        stateChangedAction(true);
        dismissDialog()
    }

    render() {
        const {consumeError, saving, checked, cities, states} = this.state
        const {
            visible, title, text, onPositivePress,
            loading,
            cancelButton,
            loadingText,
            dismissDialog,
            iconName, category, categoryUpdate, action, filter, filterUpdateAction, filterTypeAction
        } = this.props;

        const {orderBy} = filter || {}


        return (

            <Modal
                onRequestClose={() => {
                }}
                animationType='fade' transparent visible={visible}>

                <TouchableWithoutFeedback onPress={dismissDialog}>



                    <View style={blackBackground}>
                        <View style={whiteContainer}>


                            <View style={titleWrapper}>
                                <Icon
                                    size={20}
                                    name={iconName}
                                    style={iconStyle}
                                />
                                <Text style={whiteTitleStyle}> {title} </Text>

                            </View>

                            <View>
                                <View style={{padding: 10, flexDirection: 'row', justifyContent:'center'}}>

                                    <TagButton
                                        onPress={() => this.pressFilter('priority')}
                                        color={orderBy === 'priority' ? purpleTitle : titleGrey}
                                        title={'Principais'}/>


                                    <TagButton
                                        onPress={() => this.pressFilter('valueUp')}
                                        color={orderBy === 'valueUp' ? purpleTitle : titleGrey}
                                        title={'Por Valor'}/>

                                    <TagButton
                                        onPress={() => this.pressFilter('promotion')}
                                        color={orderBy === 'promotion' ? purpleTitle : titleGrey}
                                        title={'Em Promoção'}/>

                                </View>
                            </View>




                        </View>


                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }


}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        width: 400,
        paddingVertical: 10,
        fontWeight: '700',
        paddingHorizontal: 10,
        borderRadius: 4,
        color: titleColor,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        fontWeight: '700',
        paddingHorizontal: 10,
        width: 400,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: purpleTitle,


        borderRadius: 8,
        color: titleColor,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});


const mapStateToProps = (state) => {


    return {
        filter: state.filter.filter,
    };
};
export default connect(mapStateToProps, {filterTypeAction, stateChangedAction, setFilterAction})(FilterDialog)






