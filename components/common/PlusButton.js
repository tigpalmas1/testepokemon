import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colors from "./colors";
import {darkPurple, lightPurple, purpleTitle, titleGrey} from "./colors/index";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {LinearGradient} from 'expo';


const PlusButton = ({label, onPress, color, disabled, textColor}) => {
    const {buttonStyle, textStyle} = styles;

    return (


        <TouchableOpacity
            onPress={disabled ? null : onPress} style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: color || titleGrey,
            borderRadius: 15,
            width: 30,
            height: 30,

        }}>


            <Text style={[textStyle, {color: textColor || 'white',}]}>{label}</Text>


        </TouchableOpacity>

    )
}

const styles = {
    textStyle: {

        fontSize: 18,
        fontWeight: '900'
    },
    buttonStyle: {
        paddingHorizontal: 10,
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',

        borderRadius: 12,

    }
}

export {PlusButton};