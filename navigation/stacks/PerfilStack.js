import React from 'react';

import {createStackNavigator} from 'react-navigation';
import TabBarIcon from "../../components/TabBarIcon";
import PerfilScreen from '../../screens/explorer/PerfilScreen'
import CreateEditProductScreen from '../../screens/products/CreateEditProductScreen'
import AddressScreen from '../../screens/address/AddressScreen'
import ThermsScreen from '../../screens/configuration/ThermsScreen'
import ChangePasswordScreen from "../../screens/auth/ChangePasswordScreen";
import ProductsListScreen from "../../screens/products/ProductsListScreen";
import AddressListScreen from "../../screens/address/AddressListScreen";


const PerfilStack = createStackNavigator({
    User: {screen: PerfilScreen, navigationOptions: {header: null},},
    createEditProduct: {screen: CreateEditProductScreen, navigationOptions: {header: null},},
    addressList: {screen: AddressListScreen, navigationOptions: {header: null},},
    addressCreate: {screen: AddressScreen, navigationOptions: {header: null},},
    changePassword: {screen: ChangePasswordScreen, navigationOptions: {header: null},},
    productsOwner: {screen: ProductsListScreen, navigationOptions: {header: null},},
    therms: ThermsScreen

});

PerfilStack.navigationOptions = ({navigation}) => {

    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }
    return {
        tabBarVisible,
        tabBarLabel: 'Usuário',
        tabBarIcon: ({focused}) => (
            <TabBarIcon
                focused={focused}
                name={'account'}
            />
        ),
    }
};


export default PerfilStack