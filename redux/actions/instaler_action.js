import {
    CARD_IMAGE_UPDATE, CLEAR_INSTALER_LIST,
    CLEAR_SELECTED_INSTALER,
    ESTABLISHMENT_COORDENATES_UPDATE, ESTABLISHMENT_IMAGE_UPDATE,
    ESTABLISHMENT_LOADING, ESTABLISHMENT_SAVE_ERROR, ESTABLISHMENT_SAVE_SUCCESS,
    ESTABLISHMENT_UPDATE, FETCH_ESTABLISHMENT, FETCH_INSTALER, FETCH_INSTALER_LOADING, INSTALER_UPDATE, LOGIN_SUCCESS,
    SET_SELECTED_INSTALER,

} from "./types";
import {checkResponseError, getStorage, setStorage} from "../../components/common/functions/index";
import axios from 'axios'
import * as IntentLauncher from 'expo-intent-launcher'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import {baseUrl} from "../../components/common/constants/index";


export const setSelectedInstaler = (installer) => async (dispatch) => {
    dispatch({type: SET_SELECTED_INSTALER, payload: installer})
};

export const clearSelectedInstaler = () => async (dispatch) => {

    dispatch({type: CLEAR_SELECTED_INSTALER})
};

export const clearInstallerList = () => async (dispatch) => {

    dispatch({type: CLEAR_INSTALER_LIST})
};

export const cardImageUpdate = (value) => async (dispatch) => {
    dispatch({
        type: CARD_IMAGE_UPDATE,
        payload: value
    })
};


export const instalerUpdateAction = ({prop, value}) => async (dispatch) => {
    console.log('entrando aqui')
    console.log(prop)
    console.log(value)
    dispatch({
        type: INSTALER_UPDATE,
        payload: {prop, value}
    })
};

export const fetchInstalers = (skip, filter, coords) => async (dispatch) => {

    dispatch({
        type: FETCH_INSTALER_LOADING,
        payload: {loading: true, message: 'Buscando Instaladores próximos de você'}
    });


    try {
        let url = `${baseUrl}/installer?`;
        let config = '';
        const {city, state,} = filter || {};
        const {latitude, longitude} = coords || {};
        console.log(filter)
        console.log(coords)


        config = {
            params: {
                city: city,
                state: state,
                lat: latitude,
                long: longitude,
                skip: skip,
                limit: 10
            },
        };
        const {data} = await axios.get(url, config);


        dispatch({
            type: FETCH_INSTALER,
            payload: {
                instalers: data,
                message: data.length > 0 ? '' : `Sem instaladores para a cidade de ${city} encontrados no momento`,
                endList: data.length < 10,
            }
        });

    } catch (e) {

        dispatch({
            type: FETCH_INSTALER_LOADING,
            payload: {loading: false, message: checkResponseError(e)}
        });

    }
};


export const installerSaveAction = (_id, user, callback) => async (dispatch) => {

    try {
        const data = await axios.put(`${baseUrl}/user/${_id}`, user);

        await setStorage("user", data.data)
        callback('Usuário salvo com sucesso')


    } catch (e) {

        callback(checkResponseError(e))

    }
};


export const searchCep = (cep, callback) => async (dispatch) => {
    const url = `https://viacep.com.br/ws/${cep}/json`
    try {
        const {data} = await axios.get(url)
        const {logradouro, localidade, uf, bairro, cep} = data || {}
        console.log(data)

        dispatch({type: INSTALER_UPDATE, payload: {prop: 'street', value: logradouro}})
        dispatch({type: INSTALER_UPDATE, payload: {prop: 'city', value: localidade}})
        dispatch({type: INSTALER_UPDATE, payload: {prop: 'state', value: uf}})
        dispatch({type: INSTALER_UPDATE, payload: {prop: 'neighborhood', value: bairro}})
        dispatch({type: INSTALER_UPDATE, payload: {prop: 'cep', value: cep}})

        callback()


    } catch (e) {
        callback()
        checkResponseError(e)
    }
}


export const saveInstalerCover = (user, uri, callback) => async (dispatch) => {


    try {

        const form = new FormData();
        form.append('cardImage', {uri: uri, type: 'image/jpeg', name: 'cardImage'});

        var url = `${baseUrl}/user/${user._id}`;
        const {data} = await axios.put(url, form)
        await setStorage("user", data)
        const {imgLogo} = data || {}


        dispatch({type: INSTALER_UPDATE, payload: {prop: 'imgLogo', value: imgLogo}})
        callback('salvo com sucesso')


    } catch (e) {

        callback(checkResponseError(e))
    }

};


export const _getLocationAsync = () => async (dispatch) => {
    console.log('chamoou getLocationAsync');
    let {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {

        return null
    } else {

        try {
            let status = await Location.getProviderStatusAsync({});
            if (!status.locationServicesEnabled) {
                if (Platform.OS === 'ios') {
                    Linking.openURL('app-settings:');
                } else {
                    IntentLauncher.startActivityAsync(
                        IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS
                    )
                }
                console.log('null')
                return null
            } else {

                const userLocation = await Location.getCurrentPositionAsync();
                const {coords} = userLocation || {}
                const {latitude, longitude} = coords
                if (latitude) dispatch({type: INSTALER_UPDATE, payload: {prop: 'lat', value: latitude}})
                if (longitude) dispatch({type: INSTALER_UPDATE, payload: {prop: 'long', value: longitude}})

            }
        } catch (e) {
            if (e.message === "Location services are disabled") {
                alert('favor Ligar o Gps');
            }

            console.log(e.message);
            return null
        }
    }
};







