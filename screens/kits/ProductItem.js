import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {grey, titleGrey} from "../../components/common/colors/index";
import {Button} from "../../components/common/Button";
import {PlusButton} from "../../components/common/PlusButton";


const ProductItem = ({el, pressPlus, pressLess, hasButton}) => {


    const {productId, quantity, isAccessory} = el || {}
    const {model, installatorPrice,} = productId || {}




    return (

        <View style={{flexDirection: 'row', width: '100%', marginTop:5}}>
            <View style={{flex: 8}}>
                <Text style={{fontSize: 18, fontWeight: '500', color: titleGrey}}>
                    -{model}</Text>

            </View>

            {isAccessory &&
            <View style={{flex: 3, flexDirection: 'row', alignItems: 'center'}}>

                {hasButton &&
                <View>
                    <PlusButton
                        color={quantity<=0 ? grey: titleGrey}
                        onPress={quantity<=0? null: ()=>pressLess(el._id)} label={'-'}/>
                </View>
                }

                <View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{
                            fontSize: 16,
                            fontWeight: '500',
                            color: titleGrey
                        }}>{`  ${quantity}  `}</Text>
                    </View>
                </View>
                {hasButton &&
                <View>
                    <PlusButton onPress={() => pressPlus(el._id)} label={'+'}/>
                </View>
                }
            </View>
            }


            {!isAccessory &&
            <View style={{flex: 2, flexDirection: 'row', alignItems: 'center'}}>


                    <View style={{flexDirection: 'row'}}>
                        <Text style={{
                            fontSize: 16,
                            fontWeight: '500',
                            color: titleGrey
                        }}>{`  ${quantity}  `}</Text>
                    </View>
                </View>


            }



        </View>
    )
}


export {ProductItem};