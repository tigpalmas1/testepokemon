import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import Colors from '../constants/Colors';
import {grey, purpleTitle} from "./common/colors/index";

export default class TabBarIcon extends React.Component {
  render() {
    return (
        <Icon
            size={24}
            name={this.props.name}
            color={this.props.focused ? purpleTitle: grey}
        />

    );
  }
}