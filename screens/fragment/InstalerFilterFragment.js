import React from 'react';
import {StyleSheet, View, ScrollView, Text} from 'react-native';
import {connect} from "react-redux";
import {getFormatedDate, revertFilterData} from "../../components/common/functions/index";
import {GradientTagButton} from "../../components/common/GradientTagButton";


const InstalerFilterFragment = (props) => {



    const {filter, onFilterPress} = props || {};

    const {state, city, } = filter || {};


    const {container,} = styles;
    return (
        <View style={container}>
            <ScrollView
                contentContainerStyle={{alignItems: 'center',}}
                horizontal
                showsHorizontalScrollIndicator={false}
            >


                <GradientTagButton
                    onPress={onFilterPress}
                    title={`${city} - ${state}`}
                    iconName={'map-marker'}
                />


            </ScrollView>
        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: 5,
        marginTop: -10,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd',
    },
});

const mapStateToProps = (state) => {

    return {
        filter: state.filter.filter
    }
};


export default connect(mapStateToProps, null)(InstalerFilterFragment)